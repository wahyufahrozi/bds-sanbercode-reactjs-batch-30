import React, { useContext } from "react";

import "./navbar.css";
import { Row, Col, Input } from "antd";
import { Link } from "react-router-dom";
import Logo from "../../assets/logo.png";
import { GameContext } from "../../context/GameContext";
const { Search } = Input;

const Navbar = () => {
  const { searchItems } = useContext(GameContext);
  return (
    // <Affix offsetTop={0}>
    <div className="navigation">
      <Row>
        <Col span={3}>
          <img src={Logo} width={90} alt="Logo" />
        </Col>
        <Col span={2}>
          <Link to="/">Home</Link>
        </Col>
        <Col span={2}>
          <Link to="/mobilelist">Mobile List</Link>
        </Col>
        <Col span={12}>
          <Link to="/about">About</Link>
        </Col>
        <Col span={4}>
          <Search
            placeholder="input search text"
            allowClear
            enterButton="Search"
            size="medium"
            onChange={(e) => searchItems(e.target.value)}
            className="searcbox"
          />
        </Col>
      </Row>
    </div>
    // </Affix>
  );
};

export default Navbar;
