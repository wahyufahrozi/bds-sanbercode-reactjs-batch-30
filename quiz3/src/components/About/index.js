import React from "react";
import { Link } from "react-router-dom";

const About = () => {
  return (
    <div className="container">
      <div className="content">
        <h1 style={{ textAlign: "center" }}>
          Data Peserta Sanbercode Bootcamp Reactjs
        </h1>
        <ol>
          <li>
            <p>
              <b>Nama:</b> Wahyu Fahrozi Rezeki Ramadhan
            </p>
          </li>
          <li>
            <p>
              <b>Email:</b> wahyufahrozi97@gmail.com
            </p>
          </li>
          <li>
            <p>
              <b>Sistem Operasi yang digunakan:</b> Windows
            </p>
          </li>
          <li>
            <p>
              <b>Akun Gitlab:</b> https://gitlab.com/wahyufahrozi
            </p>
          </li>
          <li>
            <p>
              <b>Akun Telegram:</b> reznov1497
            </p>
          </li>
        </ol>
      </div>
      <hr />
      <button>
        <Link to="/">Kembali ke index</Link>
      </button>
    </div>
  );
};
export default About;
