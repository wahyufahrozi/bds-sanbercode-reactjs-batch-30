import axios from "axios";
import React from "react";
import { useHistory } from "react-router-dom/cjs/react-router-dom.min";
import { useContext } from "react/cjs/react.development";
import { GameContext } from "../../context/GameContext";
import { message } from "antd";

import "./form.css";
const Form = () => {
  const history = useHistory();
  const {
    input,
    setInput,
    currentId,
    setCurrentId,
    fetchData,
    checkAndroid,
    setCheckAndroid,
    checkIos,
    setCheckIos,
  } = useContext(GameContext);
  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
    console.log(value);
  };
  const successAdd = () => {
    message.success("Data berhasil ditambah!");
  };
  const successEdit = () => {
    message.success("Data berhasil diubah");
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let {
      name,
      category,
      description,
      release_year,
      size,
      price,
      rating,
      image_url,
      // is_android_app,
      // is_ios_app,
    } = input;
    // const testData = {
    //   name,
    //   category,
    //   description,
    //   release_year,
    //   size,
    //   price,
    //   rating,
    //   image_url,
    //   is_android_app: checkAndroid,
    //   is_ios_app: checkIos,
    // };
    // console.log(testData);
    if (currentId === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/mobile-apps`, {
          name,
          category,
          description,
          release_year,
          size,
          price,
          rating,
          image_url,
          is_android_app: checkAndroid,
          is_ios_app: checkIos,
        })
        .then((res) => {
          fetchData();
          submitButton();
          successAdd();
        });
      setInput({
        name: "",
        category: "",
        description: "",
        release_year: "",
        size: "",
        price: "",
        rating: "",
        image_url: "",
        is_android_app: "",
        is_ios_app: "",
      });
    } else {
      axios
        .put(
          `http://backendexample.sanbercloud.com/api/mobile-apps/${currentId}`,
          {
            name,
            category,
            description,
            release_year,
            size,
            price,
            rating,
            image_url,
            is_android_app: checkAndroid,
            is_ios_app: checkIos,
          }
        )
        .then(() => {
          fetchData();
          submitButton();
          successEdit();
        });
      setCurrentId(null);
      setInput({
        name: "",
        category: "",
        description: "",
        release_year: "",
        size: "",
        price: "",
        rating: "",
        image_url: "",
        is_android_app: "",
        is_ios_app: "",
      });
    }
  };

  function submitButton() {
    history.push("/mobilelist");
  }
  return (
    <div className="container">
      <div className="content">
        <form onSubmit={handleSubmit}>
          <label>Name</label>
          <input
            type="text"
            value={input.name}
            name="name"
            required
            onChange={handleChange}
          />
          <label>Category</label>
          <input
            type="text"
            value={input.category}
            name="category"
            required
            onChange={handleChange}
          />
          <label>Description</label>
          <textarea
            value={input.description}
            name="description"
            required
            onChange={handleChange}
          />
          <label>Year</label>
          <input
            type="number"
            value={input.release_year}
            name="release_year"
            min={2007}
            max={2021}
            placeholder="2007"
            required
            onChange={handleChange}
          />
          <label>Size(MB)</label>
          <input
            type="text"
            value={input.size}
            name="size"
            required
            onChange={handleChange}
          />
          <label>Price</label>
          <input
            type="number"
            value={input.price}
            name="price"
            required
            onChange={handleChange}
          />
          <label>Rating</label>
          <input
            type="number"
            value={input.rating}
            name="rating"
            required
            onChange={handleChange}
            min={0}
            max={5}
          />
          <label>Img url</label>
          <input
            type="text"
            value={input.image_url}
            name="image_url"
            required
            onChange={handleChange}
          />
          <label>Platfrom</label>
          <br />
          <label for="android">
            <b>Android </b>
          </label>
          <input
            type="checkbox"
            id="android"
            name="android"
            checked={checkAndroid}
            onChange={(e) => {
              setCheckAndroid(e.target.checked);
            }}
          />
          <br />
          <label for="ios">
            <b>IOS </b>
          </label>
          <input
            type="checkbox"
            id="ios"
            name="ios"
            checked={checkIos}
            onChange={(e) => {
              setCheckIos(e.target.checked);
            }}
          />
          <br />
          <input type="submit" value="Submit" />
        </form>
      </div>
    </div>
  );
};

export default Form;
