import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
import {
  singlePlayerStatus,
  multiplayerStatus,
} from "../components/utils/player";

export const DataContext = createContext();

export const DataProvider = (props) => {
  const [daftarDataMovie, setDaftarDataMovie] = useState([]);
  const [daftarDataGame, setDaftarDataGame] = useState([]);
  const [input, setInput] = useState("");
  const [searchInput, setSearchInput] = useState("");
  const [filteredResultMovie, setFilteredResultMovie] = useState([]);
  const [filteredResultGame, setFilteredResultGame] = useState([]);
  const [isLoading, setIsLoading] = useState(true);
  const [currentId, setCurrentId] = useState(null);
  const [loginStatus, setLoginStatus] = useState(false);

  const searchItemMovie = (search) => {
    setSearchInput(search);
    if (searchInput !== "") {
      const filteredData = daftarDataMovie.filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResultMovie(filteredData);
    } else {
      setFilteredResultMovie(daftarDataMovie);
    }
  };
  const filterMovie = (year, duration, rating) => {
    let datafilter = daftarDataMovie
      .filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(year.toLowerCase());
      })
      .filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(duration.toLowerCase());
      })
      .filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(rating.toLowerCase());
      });
    setDaftarDataMovie(datafilter);
  };

  const filterGame = (genre, release) => {
    let daftarFiltergame = daftarDataGame
      .filter((item) => {
        return Object.values(item).join("").toLocaleLowerCase().includes(genre);
      })
      .filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(release);
      });

    setDaftarDataMovie(daftarFiltergame);
  };
  const searchItemGame = (search) => {
    setSearchInput(search);
    if (searchInput !== "") {
      const filteredData = daftarDataGame.filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResultGame(filteredData);
    } else {
      setFilteredResultGame(daftarDataGame);
    }
  };

  const fetchDataMovie = async () => {
    const result = await axios.get(
      `https://backendexample.sanbersy.com/api/data-movie`
    );
    setDaftarDataMovie(
      result.data.map((item) => {
        return {
          id: item.id,
          title: item.title,
          description: item.description,
          year: item.year,
          duration: item.duration,
          genre: item.genre,
          rating: item.rating,
          review: item.review,
          image_url: item.image_url,
        };
      })
    );
  };

  const fetchDataGame = async () => {
    const result = await axios.get(
      `https://backendexample.sanbersy.com/api/data-game`
    );
    setDaftarDataGame(
      result.data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          singlePlayer: singlePlayerStatus(item.singlePlayer),
          multiplayer: multiplayerStatus(item.multiplayer),
          platform: item.platform,
          release: item.release,
          genre: item.genre,
          image_url: item.image_url,
        };
      })
    );
  };
  // console.log(daftarDataMovie);
  useEffect(() => {
    setIsLoading(false);
    fetchDataMovie();
    fetchDataGame();
  }, []);
  return (
    <DataContext.Provider
      value={{
        daftarDataMovie,
        setDaftarDataMovie,
        daftarDataGame,
        setDaftarDataGame,
        input,
        setInput,
        searchInput,
        setSearchInput,
        searchItemMovie,
        searchItemGame,
        filteredResultMovie,
        filteredResultGame,
        currentId,
        setCurrentId,
        loginStatus,
        setLoginStatus,
        fetchDataMovie,
        fetchDataGame,
        isLoading,
        setIsLoading,
        filterMovie,
        filterGame,
      }}
    >
      {props.children}
    </DataContext.Provider>
  );
};
