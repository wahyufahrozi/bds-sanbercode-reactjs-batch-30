import {
  BrowserRouter as Router,
  Route,
  Switch,
  Redirect,
} from "react-router-dom";
import Contents from "../components/Contents";
import DetailMovie from "../components/Detail/Movie";
import DetailGame from "../components/Detail/Game";
import FooterComp from "../components/Footer";
import Navbar from "../components/Navbar";
import ListGame from "../components/Contents/ListGame";
import ListMovie from "../components/Contents/ListMovie";
import SignIn from "../components/Signin";
import AdminMovie from "../components/Admin/Movie";
import AdminGame from "../components/Admin/Game";
import MoviesForm from "../components/Form/MoviesForm";
import MoviesFormEdit from "../components/Form/MoviesForm";
import Cookies from "js-cookie";
import Signup from "../components/Signup";
import GamesForm from "../components/Form/GameForm";
import GamesFormEdit from "../components/Form/GameForm";
import ChangePassword from "../components/Form/ChangePassword";
const Routes = () => {
  const LoginRoute = ({ ...props }) => {
    if (Cookies.get("token") !== undefined) {
      return <Redirect to="/" />;
    } else {
      return <Route {...props} />;
    }
  };
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={Contents} />
          <LoginRoute path="/signin" exact component={SignIn} />
          <LoginRoute path="/signup" exact component={Signup} />
          <Route path="/movielist" exact component={ListMovie} />
          <Route path="/gamelist" exact component={ListGame} />
          <Route path="/movie/detail/:id" exact component={DetailMovie} />
          <Route path="/game/detail/:id" exact component={DetailGame} />
          <Route path="/admin/movielist" exact component={AdminMovie} />
          <Route path="/admin/gamelist" exact component={AdminGame} />
          <Route path="/admin/createmovie" exact component={MoviesForm} />
          <Route path="/admin/editmovie" exact component={MoviesFormEdit} />
          <Route path="/admin/creategame" exact component={GamesForm} />
          <Route path="/admin/editgame" exact component={GamesFormEdit} />
          <Route
            path="/admin/changepassword"
            exact
            component={ChangePassword}
          />
        </Switch>
        <FooterComp />
      </Router>
    </>
  );
};

export default Routes;
