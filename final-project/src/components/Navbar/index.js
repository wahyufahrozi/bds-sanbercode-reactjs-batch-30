import Cookies from "js-cookie";
import React, { useContext } from "react";
import { Layout, Menu } from "antd";
import Logo from "../../assets/img/logo.png";
import { Link } from "react-router-dom";
import { DataContext } from "../../context/DataContext";
import { useHistory } from "react-router";
const { Header } = Layout;
const Navbar = () => {
  // const [selectedMenuItem, setSelectedMenuItem] = useState("item1");
  const { setLoginStatus } = useContext(DataContext);
  let history = useHistory();
  const handleLogout = () => {
    setLoginStatus(false);
    Cookies.remove("user");
    Cookies.remove("email");
    Cookies.remove("token");
    history.push("/signin");
  };
  // console.log("sadsd", Cookies.get("user"));
  return (
    <Layout className="layout">
      <Header
        style={{
          position: "fixed",
          zIndex: 1,
          width: "100%",
          display: "flex",
          justifyContent: "space-between",
        }}
      >
        <img src={Logo} alt="Logo" height={60} className="logo" />
        <Menu
          theme="dark"
          mode="horizontal"
          defaultSelectedKeys={["1"]}
          style={{ minWidth: "340px" }}
        >
          <Menu.Item key={"1"}>
            <Link to="/">Home</Link>
          </Menu.Item>
          <Menu.Item key={"2"}>
            <Link to="/movielist">Movies</Link>
          </Menu.Item>
          <Menu.Item key={"3"}>
            <Link to="/gamelist">Games</Link>
          </Menu.Item>
          {Cookies.get("token") !== undefined && (
            <Menu.Item onClick={handleLogout}>Logout</Menu.Item>
          )}
          {Cookies.get("token") === undefined && (
            <Menu.Item key={"4"}>
              <Link to="/signin">Login</Link>
            </Menu.Item>
          )}
        </Menu>
      </Header>
    </Layout>
  );
};
export default Navbar;
