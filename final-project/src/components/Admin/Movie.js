import React, { useContext, useState } from "react";
import Container from "../container";
import { Button, Col, Row, Select, Input } from "antd";
import TableMovies from "./TableMovies";
import { DataContext } from "../../context/DataContext";
import { Link } from "react-router-dom";

function searching(val) {
  console.log("search:", val);
}
const { Option } = Select;
const AdminMovie = () => {
  const { searchItemMovie, daftarDataMovie, filterMovie, fetchDataMovie } =
    useContext(DataContext);

  const [year, setYear] = useState("");
  const [rating, setRating] = useState("");
  const [duration, setDuration] = useState("");

  const uniqueYear = [
    ...new Map(daftarDataMovie.map((item) => [item.year, item])).values(),
  ];
  const uniqueRating = [
    ...new Map(daftarDataMovie.map((item) => [item.rating, item])).values(),
  ];
  const uniqueDuration = [
    ...new Map(daftarDataMovie.map((item) => [item.duration, item])).values(),
  ];
  // let datas = duration.data.sort(function (a, b) {
  //   return a.duration - b.duration;
  // });
  // console.log("dasd", datas);
  function onChange(value) {
    setYear(value);
    setRating(value);
    setDuration(value);
  }
  const handleFilter = () => {
    setYear("");
    setRating("");
    setDuration("");
    fetchDataMovie();
  };

  return (
    <Container>
      <div className="admin-movie-container">
        <h1 style={{ lineHeight: "100px" }}>
          <b>Movies Filter</b>
        </h1>
        <Row gutter={16} style={{ marginBottom: "10px" }}>
          <Col span={8}>
            <h2>
              <b>Year Movies</b>
            </h2>
            <Select
              showSearch
              placeholder="Select a Year"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={searching}
              style={{ width: "100%" }}
            >
              {uniqueYear.map((item, index) => {
                return (
                  <Option value={`${item.year}`} key={index}>
                    {item.year}
                  </Option>
                );
              })}
            </Select>
          </Col>
          <Col span={8}>
            <h2>
              <b>Rating Movies</b>
            </h2>
            <Select
              showSearch
              placeholder="Select a Rating"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={searching}
              style={{ width: "100%" }}
            >
              {uniqueRating.map((item, index) => {
                return (
                  <Option value={`${item.rating}`} key={index}>
                    {item.rating}
                  </Option>
                );
              })}
            </Select>
          </Col>
          <Col span={8}>
            <h2>
              <b>Duration</b>
            </h2>
            <Select
              showSearch
              placeholder="Select a Duration"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={searching}
              style={{ width: "100%" }}
            >
              {uniqueDuration.map((item, index) => {
                return (
                  <Option value={`${item.duration}`} key={index}>
                    {item.duration}
                  </Option>
                );
              })}
            </Select>
          </Col>
        </Row>
        <Button
          onClick={() => filterMovie(year, rating, duration)}
          type="primary"
          style={{
            borderRadius: "5px",
            marginRight: "10px",
            height: "40px",
            fontSize: "15px",
          }}
        >
          Filter
        </Button>
      </div>

      <Row>
        <Col span={9}>
          <Input
            placeholder="Cari Berdasarkan title movies..."
            style={{ width: "50%" }}
            onChange={(e) => searchItemMovie(e.target.value)}
          />
        </Col>
        <Col span={5} offset={10}>
          <Button
            onClick={() => handleFilter()}
            type="primary"
            style={{
              borderRadius: "5px",
              marginRight: "15px",
              height: "40px",
              fontSize: "15px",
            }}
            danger
          >
            Clear Sort
          </Button>
          <Link to="/admin/createmovie">
            <Button
              type="primary"
              style={{
                borderRadius: "5px",
                height: "40px",
                fontSize: "15px",
              }}
            >
              Create New Movies
            </Button>
          </Link>
        </Col>
      </Row>

      <TableMovies />
    </Container>
  );
};
export default AdminMovie;
