import React, { useContext, useState, useEffect } from "react";
import Container from "../container";
import { Button, Col, Row, Select, Input, Radio } from "antd";
import { DataContext } from "../../context/DataContext";
import { Link } from "react-router-dom";
import TableGames from "./TableGames";

const AdminGame = () => {
  const { searchItemGame, daftarDataGame, fetchDataGame, filterGame } =
    useContext(DataContext);
  // const [value, setValue] = useState(false);
  const [genre, setGenre] = useState("");
  const [release, setRelease] = useState("");
  const [multi, setmulti] = useState(false);

  const radioValue = (e) => {
    // console.log("radio checked", e.target.value);
    // setValue(e.target.value);
    setmulti(e.target.value);
  };

  function onChange(value) {
    setGenre(value);
    console.log("dasd", value);
    setRelease(value);
  }

  function onSearch(val) {
    console.log("search:", val);
  }
  const { Option } = Select;
  const uniqueGenre = [
    ...new Map(daftarDataGame.map((item) => [item.genre, item])).values(),
  ];
  const uniqueRelease = [
    ...new Map(daftarDataGame.map((item) => [item.release, item])).values(),
  ];
  // useEffect(() => {
  //   fetchDataGame();
  // }, []);
  return (
    <Container>
      <div className="admin-movie-container">
        <h1 style={{ lineHeight: "100px" }}>
          <b>Games Filter</b>
        </h1>
        <Row gutter={16} style={{ marginBottom: "10px" }}>
          <Col span={8}>
            <h2>
              <b>Genre</b>
            </h2>
            <Select
              showSearch
              placeholder="Select a Genre"
              optionFilterProp="children"
              onChange={onChange}
              onSearch={onSearch}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              style={{ width: "100%" }}
            >
              {uniqueGenre.map((item, index) => {
                return (
                  <Option value={`${item.genre}`} key={index}>
                    {item.genre}
                  </Option>
                );
              })}
            </Select>
          </Col>
          <Col span={8}>
            <h2>
              <b>Release Year</b>
            </h2>
            <Select
              showSearch
              placeholder="Select a Year"
              optionFilterProp="children"
              onSearch={onSearch}
              onChange={onChange}
              filterOption={(input, option) =>
                option.children.toLowerCase().indexOf(input.toLowerCase()) >= 0
              }
              style={{ width: "100%" }}
            >
              {uniqueRelease.map((item, index) => {
                return (
                  <Option value={`${item.release}`} key={index}>
                    {item.release}
                  </Option>
                );
              })}
            </Select>
          </Col>
          <Col span={8}>
            <h2>
              <b>Multiplayer</b>
            </h2>
            <Radio.Group onChange={radioValue} value={multi}>
              <Radio value={true}>Yes</Radio>
              <Radio value={false}>No</Radio>
              <Radio value={3}>All</Radio>
            </Radio.Group>
          </Col>
        </Row>
        <Button
          onClick={() => filterGame(genre, release)}
          type="primary"
          style={{
            borderRadius: "5px",
            marginRight: "10px",
            height: "40px",
            fontSize: "15px",
          }}
        >
          Filter
        </Button>
      </div>

      <Row>
        <Col span={9}>
          <Input
            placeholder="Cari Berdasarkan title Games..."
            style={{ width: "50%" }}
            onChange={(e) => searchItemGame(e.target.value)}
          />
        </Col>
        <Col span={5} offset={10}>
          <Button
            onClick={() => fetchDataGame()}
            type="primary"
            style={{
              borderRadius: "5px",
              marginRight: "15px",
              height: "40px",
              fontSize: "15px",
            }}
            danger
          >
            Clear Sort
          </Button>
          <Link to="/admin/creategame">
            <Button
              type="primary"
              style={{
                borderRadius: "5px",

                height: "40px",
                fontSize: "15px",
              }}
            >
              Create New Movies
            </Button>
          </Link>
        </Col>
      </Row>

      <TableGames />
    </Container>
  );
};
export default AdminGame;
