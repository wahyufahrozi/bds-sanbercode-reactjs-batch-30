import React, { useContext } from "react";
import { DataContext } from "../../context/DataContext";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import { message, Table, Space, Tooltip } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import Skeletons from "../utils/SkeletonAdmin";

const TableGames = () => {
  let history = useHistory();

  const {
    daftarDataGame,
    searchInput,
    filteredResultGame,
    fetchDataGame,
    setCurrentId,
    setInput,
    isLoading,
  } = useContext(DataContext);
  const textDelete = <span>Delete</span>;
  const textEdit = <span>Edit</span>;

  const successDelete = () => {
    message.success("Data Terhapus");
  };
  const handleDelete = (record) => {
    let idGame = record.id;
    console.log(idGame);
    axios
      .delete(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
      })
      .then(() => {
        fetchDataGame();
        successDelete();
      });
  };

  const handleEdit = (record) => {
    let idGame = record.id;
    axios
      .get(`https://backendexample.sanbersy.com/api/data-game/${idGame}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
      })
      .then((result) => {
        let data = result.data;
        setInput({
          name: data.name,
          genre: data.genre,
          image_url: data.image_url,
          release: data.release,
          platform: data.platform,
          singlePlayer: data.singlePlayer,
          multiplayer: data.multiplayer,
        });
        setCurrentId(data.id);
        editButton();
      });
  };
  function editButton() {
    history.push("/admin/editgame");
  }
  const column = [
    {
      title: "No",
      key: "index",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Cover",
      key: "key",
      render: (t, r) => <img src={`${r.image_url}`} alt="Cover" width={80} />,
    },
    {
      title: "Name",
      dataIndex: "name",
      key: "key",
      onFilter: (value, record) => record.name.indexOf(value) === 0,
      sorter: (a, b) => a.name.length - b.name.length,
      sortDirections: ["descend"],
    },
    {
      title: "Genre",
      dataIndex: "genre",
      key: "key",
      sorter: (a, b) => a.genre - b.genre,
    },
    {
      title: "Release",
      dataIndex: "release",
      key: "key",
      sorter: (a, b) => a.release - b.release,
    },
    {
      title: "Platform",
      dataIndex: "platform",
      key: "key",
      sorter: (a, b) => a.duration - b.duration,
    },
    {
      title: "Multiplayer",
      key: "key",
      dataIndex: "multiplayer",
      sorter: (a, b) => a.multiplayer - b.multiplayer,
    },
    {
      title: "Singleplayer",
      key: "key",
      dataIndex: "singlePlayer",
      sorter: (a, b) => a.singlePlayer - b.singlePlayer,
    },

    {
      title: "Aksi",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Tooltip placement="top" title={textEdit} color="green">
            <button className="edit-button" onClick={() => handleEdit(record)}>
              <EditOutlined />
            </button>
          </Tooltip>
          <Tooltip placement="top" title={textDelete} color="red">
            <button
              className="delete-button"
              onClick={() => handleDelete(record)}
            >
              <DeleteOutlined />
            </button>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <>
      {isLoading ? (
        <>
          <Skeletons />
          <br />
          <Skeletons />
          <br />
          <Skeletons />
          <br />
          <Skeletons />
        </>
      ) : (
        <Table
          dataSource={
            searchInput.length > 1 ? filteredResultGame : daftarDataGame
          }
          columns={column}
        />
      )}
    </>
  );
};
export default TableGames;
