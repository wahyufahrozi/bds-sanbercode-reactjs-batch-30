import React, { useContext } from "react";
import { DataContext } from "../../context/DataContext";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { useHistory } from "react-router-dom";

import { message, Table, Space, Tooltip } from "antd";
import axios from "axios";
import Cookies from "js-cookie";
import Skeletons from "../utils/SkeletonAdmin";

const TableMovies = () => {
  let history = useHistory();

  const {
    daftarDataMovie,
    searchInput,
    filteredResultMovie,
    fetchDataMovie,
    setCurrentId,
    setInput,
    isLoading,
  } = useContext(DataContext);
  const textDelete = <span>Delete</span>;
  const textEdit = <span>Edit</span>;

  const successDelete = () => {
    message.success("Data Terhapus");
  };
  const handleDelete = (record) => {
    let idMovies = record.id;
    console.log(idMovies);
    axios
      .delete(
        `https://backendexample.sanbersy.com/api/data-movie/${idMovies}`,
        { headers: { Authorization: "Bearer " + Cookies.get("token") } }
      )
      .then(() => {
        fetchDataMovie();
        successDelete();
      });
  };

  const handleEdit = (record) => {
    let idMovies = record.id;
    axios
      .get(`https://backendexample.sanbersy.com/api/data-movie/${idMovies}`, {
        headers: { Authorization: "Bearer " + Cookies.get("token") },
      })
      .then((result) => {
        let data = result.data;
        setInput({
          title: data.title,
          description: data.description,
          genre: data.genre,
          rating: data.rating,
          review: data.review,
          year: data.year,
          image_url: data.image_url,
          duration: data.duration,
        });
        setCurrentId(data.id);
        editButton();
      });
  };
  function editButton() {
    history.push("/admin/editmovie");
  }
  const column = [
    {
      title: "No",
      key: "index",
      render: (text, record, index) => index + 1,
    },
    {
      title: "Cover",
      key: "key",
      render: (t, r) => <img src={`${r.image_url}`} alt="Cover" width={80} />,
    },
    {
      title: "Title",
      dataIndex: "title",
      key: "key",
      onFilter: (value, record) => record.title.indexOf(value) === 0,
      sorter: (a, b) => a.title.length - b.title.length,
      sortDirections: ["descend"],
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "key",
      ellipsis: {
        showTitle: false,
      },
      render: (description) => (
        <Tooltip placement="topLeft" title={description}>
          {description}
        </Tooltip>
      ),

      sorter: (a, b) => a.description.length - b.description.length,
    },
    {
      title: "Year",
      dataIndex: "year",
      key: "key",
      sorter: (a, b) => a.year - b.year,
    },
    {
      title: "Duration",
      dataIndex: "duration",
      key: "key",
      sorter: (a, b) => a.duration - b.duration,
    },
    {
      title: "Genre",
      dataIndex: "genre",
      key: "key",
      sorter: (a, b) => a.genre - b.genre,
    },
    {
      title: "Rating",
      key: "key",
      render: (t, r) => <p>{`${r.rating}/10`}</p>,
      sorter: (a, b) => a.rating - b.rating,
    },

    {
      title: "Review",
      dataIndex: "review",
      sorter: (a, b) => a.review.length - b.review.length,
    },
    {
      title: "Aksi",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Tooltip placement="top" title={textEdit} color="green">
            <button className="edit-button" onClick={() => handleEdit(record)}>
              <EditOutlined />
            </button>
          </Tooltip>
          <Tooltip placement="top" title={textDelete} color="red">
            <button
              className="delete-button"
              onClick={() => handleDelete(record)}
            >
              <DeleteOutlined />
            </button>
          </Tooltip>
        </Space>
      ),
    },
  ];

  return (
    <>
      {isLoading ? (
        <>
          <Skeletons />
          <br />
          <Skeletons />
          <br />
          <Skeletons />
          <br />
          <Skeletons />
        </>
      ) : (
        <Table
          dataSource={
            searchInput.length > 1 ? filteredResultMovie : daftarDataMovie
          }
          columns={column}
        />
      )}
    </>
  );
};
export default TableMovies;
