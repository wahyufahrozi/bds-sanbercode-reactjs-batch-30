import React, { useContext } from "react";
import { Col, Row } from "antd";
import Container from "../container";
import Login from "../../assets/img/logo-login.jpg";

import "./signin.css";
import axios from "axios";
import { DataContext } from "../../context/DataContext";
import { useHistory } from "react-router-dom";

const Signup = () => {
  let history = useHistory();
  const { input, setInput } = useContext(DataContext);
  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
    console.log(value);
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let { name, email, password } = input;
    axios
      .post(`https://backendexample.sanbersy.com/api/register`, {
        name,
        email,
        password,
      })
      .then((res) => {
        let data = res.data;
        console.log(data);
        submitButton();
      })
      .catch((error) => {
        console.log(error);
      });
  };
  const submitButton = () => {
    history.push("/signin");
  };
  return (
    <Container>
      <div className="content">
        <Row>
          <Col span={12}>
            <h1>Masuk Akun Anda</h1>
            <div className="form-signin">
              <form onSubmit={handleSubmit}>
                <label for="name">name</label>
                <input
                  type="text"
                  id="name"
                  value={input.name}
                  onChange={handleChange}
                  name="name"
                  placeholder="Your name"
                />
                <label for="email">Email</label>
                <input
                  type="text"
                  id="email"
                  name="email"
                  placeholder="Your email"
                  value={input.email}
                  onChange={handleChange}
                />

                <label for="password">Password</label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  placeholder="Your Password"
                  value={input.password}
                  onChange={handleChange}
                />

                <input type="submit" value="Submit" />
              </form>
            </div>
          </Col>
          <Col span={12}>
            <img src={Login} alt="Logo Login" />
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default Signup;
