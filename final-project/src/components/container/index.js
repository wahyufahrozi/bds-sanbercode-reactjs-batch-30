import React, { useContext } from "react";

import Cookies from "js-cookie";
import { Layout, Row, Col, Menu } from "antd";
import { useHistory } from "react-router";
import { DataContext } from "../../context/DataContext";
import {
  UnorderedListOutlined,
  DesktopOutlined,
  LogoutOutlined,
  FolderOpenOutlined,
} from "@ant-design/icons";
import { Link } from "react-router-dom";

const { Content } = Layout;
const Container = ({ children }) => {
  const { setLoginStatus } = useContext(DataContext);
  let history = useHistory();
  const handleLogout = () => {
    setLoginStatus(false);
    Cookies.remove("user");
    Cookies.remove("email");
    Cookies.remove("token");
    history.push("/signin");
  };

  return (
    <Layout className="layout">
      {Cookies.get("token") !== undefined && (
        <Row gutter={16}>
          <Col span={3}>
            <Content>
              <div
                className="header-menu-admin"
                style={{ position: "fixed", left: "10px" }}
              >
                <h2>Hi {Cookies.get("user")}</h2>
                <Menu
                  defaultSelectedKeys={["1"]}
                  defaultOpenKeys={["sub1"]}
                  // mode="inline"
                  theme="light"
                >
                  <Menu.Item key="1" icon={<UnorderedListOutlined />}>
                    <Link to="/admin/movielist">Setting Movies</Link>
                  </Menu.Item>
                  <Menu.Item key="2" icon={<DesktopOutlined />}>
                    <Link to="/admin/gamelist">Setting Games</Link>
                  </Menu.Item>
                  <Menu.Item key="3" icon={<FolderOpenOutlined />}>
                    <Link to="/admin/changepassword">Change Password</Link>
                  </Menu.Item>
                  <Menu.Item
                    key="4"
                    icon={<LogoutOutlined />}
                    onClick={handleLogout}
                  >
                    Logout
                  </Menu.Item>
                </Menu>
              </div>
            </Content>
          </Col>
          <Col span={21}>
            <Content style={{ padding: "0 50px" }}>
              <div className="site-layout-content">{children}</div>
            </Content>
          </Col>
        </Row>
      )}
      {Cookies.get("token") === undefined && (
        <Content style={{ padding: "0 50px" }}>
          <div className="site-layout-content">{children}</div>
        </Content>
      )}
    </Layout>
  );
};
export default Container;
