import React from "react";
import { Layout } from "antd";
const { Footer } = Layout;

const FooterComp = () => {
  return (
    <Footer style={{ textAlign: "center" }}>
      Movie App ©2021 Created by Wahyu fahrozi
    </Footer>
  );
};
export default FooterComp;
