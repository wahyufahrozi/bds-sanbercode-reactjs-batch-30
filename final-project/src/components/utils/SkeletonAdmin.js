import React, { useContext } from "react";
import { Card, Skeleton } from "antd";
import { DataContext } from "../../context/DataContext";
const { Meta } = Card;

const Skeletons = () => {
  const { isLoading } = useContext(DataContext);
  return (
    <>
      <Skeleton avatar active loading={isLoading}>
        <Card
          hoverable
          style={{ width: 310 }}
          cover={<img alt="example" src={"sdfs"} />}
        >
          <Meta title="" style={{ marginBottom: 10 }} />
          <p>Genre :</p>
          <p>Release </p>
        </Card>
      </Skeleton>
    </>
  );
};

export default Skeletons;
