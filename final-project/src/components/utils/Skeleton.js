import React, { useContext } from "react";
import { Card, Col, Skeleton } from "antd";
import { DataContext } from "../../context/DataContext";
const { Meta } = Card;

const Skeletons = () => {
  const { isLoading } = useContext(DataContext);
  return (
    <>
      <Col className="gutter-row" span={6}>
        <Skeleton avatar active loading={isLoading}>
          <Card
            hoverable
            style={{ width: 310 }}
            cover={<img alt="example" src={"sdfs"} />}
          >
            <Meta title="" style={{ marginBottom: 10 }} />
            <p>Genre :</p>
            <p>Release </p>
          </Card>
        </Skeleton>
      </Col>
    </>
  );
};

export default Skeletons;
