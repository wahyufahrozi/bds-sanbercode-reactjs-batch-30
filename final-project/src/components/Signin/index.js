import React, { useContext, useState } from "react";
import { Col, Row, message } from "antd";
import Container from "../container";
import Login from "../../assets/img/logo-login.jpg";
import { Link } from "react-router-dom";
import axios from "axios";
import Cookies from "js-cookie";
import { useHistory } from "react-router";
import { DataContext } from "../../context/DataContext";
import "./signin.css";
const SignIn = () => {
  let history = useHistory();
  const { setLoginStatus } = useContext(DataContext);
  const [input, setInput] = useState({
    email: "",
    password: "",
  });
  const successLogin = () => {
    message.success(`Selamat Datang ${Cookies.get("user")}`);
  };

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
    console.log(value);
  };
  const handleSubmit = (event) => {
    let { email, password } = input;
    event.preventDefault();
    axios
      .post("https://backendexample.sanbersy.com/api/user-login", {
        email,
        password,
      })
      .then((res) => {
        let user = res.data.user;
        let token = res.data.token;
        Cookies.set("user", user.name, { expires: 1 });
        Cookies.set("email", user.email, { expires: 1 });
        Cookies.set("token", token, { expires: 1 });
        history.push("/");
        setLoginStatus(true);
        successLogin();
      })
      .catch((err) => {
        alert(err);
      });
  };

  return (
    <Container>
      <div className="content" onSubmit={handleSubmit}>
        <Row>
          <Col span={12}>
            <h1>Masuk Akun Anda</h1>
            <div className="form-signin">
              <form>
                <label for="email">Email</label>
                <input
                  type="text"
                  id="email"
                  name="email"
                  placeholder="Your email"
                  value={input.email}
                  onChange={handleChange}
                />

                <label for="password">Password</label>
                <input
                  type="password"
                  id="password"
                  name="password"
                  placeholder="Your Password"
                  value={input.password}
                  onChange={handleChange}
                />

                <input type="submit" value="Submit" />
              </form>
              Don't have an account?<Link to="/signup"> Sign up</Link>
            </div>
          </Col>
          <Col span={12}>
            <img src={Login} alt="Logo Login" />
          </Col>
        </Row>
      </div>
    </Container>
  );
};

export default SignIn;
