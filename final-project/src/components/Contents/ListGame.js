import React, { useContext } from "react";
import CardGames from "../Card/Game";
import Container from "../container";
import { DataContext } from "../../context/DataContext";

const ListGame = () => {
  const { daftarDataGame } = useContext(DataContext);

  return (
    <Container>
      <h2>All Games</h2>
      <CardGames daftarDataGame={daftarDataGame} />
    </Container>
  );
};

export default ListGame;
