import React, { useContext } from "react";
import CardMovie from "../Card/Movie";
import { DataContext } from "../../context/DataContext";
import Container from "../container";
import CardGames from "../Card/Game";
import { Link } from "react-router-dom";

const Contents = () => {
  const { daftarDataMovie, daftarDataGame } = useContext(DataContext);
  return (
    <Container>
      <div className="header-list">
        <h2>Latest Movies </h2>
        <Link to="/movielist">
          <h3>View More</h3>
        </Link>
      </div>
      <CardMovie daftarDataMovie={daftarDataMovie} />
      <div className="header-list">
        <h2>Latest Games </h2>
        <Link to="/gamelist">
          <h3>View More</h3>
        </Link>
      </div>
      <CardGames daftarDataGame={daftarDataGame} />
    </Container>
  );
};
export default Contents;
