import React, { useContext } from "react";
import Container from "../container";
import { DataContext } from "../../context/DataContext";
import CardMovie from "../Card/Movie";
const ListMovie = () => {
  const { daftarDataMovie } = useContext(DataContext);
  return (
    <Container>
      <h2>All Movies</h2>
      <CardMovie daftarDataMovie={daftarDataMovie} />
    </Container>
  );
};
export default ListMovie;
