import React, { useEffect, useState } from "react";
import axios from "axios";
import Container from "../container";
import { Row, Col, Divider } from "antd";
import { singlePlayerStatus, multiplayerStatus } from "../utils/player";
const Detail = (props) => {
  const [post, setPost] = useState({
    name: "",
    release: "",
    duration: "",
    genre: "",
    platform: "",
    image_url: "",
    singlePlayer: "",
    multiplayer: "",
  });

  useEffect(() => {
    let id = props.match.params.id;
    axios
      .get(`https://backendexample.sanbersy.com/api/data-game/${id}`)
      .then((res) => {
        let data = res.data;
        setPost({
          name: data.name,
          description: data.description,
          duration: data.duration,
          release: data.release,
          genre: data.genre,
          platform: data.platform,
          image_url: data.image_url,
          singlePlayer: singlePlayerStatus(data.singlePlayer),
          multiplayer: multiplayerStatus(data.multiplayer),
        });
      });
  }, []);
  return (
    <Container>
      <Row>
        <Col span={6}>
          <img
            src={post.image_url}
            alt={post.name}
            width={230}
            height={200}
            style={{ overflow: "hidden", objectFit: "cover" }}
          />
        </Col>
        <Col span={18}>
          <h1>
            {post.name} ({post.release})
          </h1>
          <Divider />
          <Row>
            <Col span={4} className="detail-info">
              <p>Year</p> <p>Genre</p> <p>Platform</p> <p>Single Player</p>
              <p>Multi Player</p>
            </Col>
            <Col span={2} className="detail-info">
              <p>:</p> <p>:</p> <p>:</p> <p>:</p>
              <p>:</p>
            </Col>
            <Col span={15} className="detail-info">
              <p>{post.release} </p>
              <p>{post.genre}</p>
              <p>{post.platform}</p>
              <p>{post.singlePlayer}</p>
              <p>{post.multiplayer}</p>
            </Col>
          </Row>
        </Col>
      </Row>
    </Container>
  );
};
export default Detail;
