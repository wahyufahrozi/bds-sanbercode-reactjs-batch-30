import React, { useEffect, useState } from "react";
import axios from "axios";
import Container from "../container";
import { Row, Col, Divider } from "antd";

const DetailMovie = (props) => {
  const [post, setPost] = useState({
    title: "",
    description: "",
    year: "",
    duration: "",
    genre: "",
    image_url: "",
    review: "",
    rating: "",
  });

  useEffect(() => {
    let id = props.match.params.id;
    axios
      .get(`https://backendexample.sanbersy.com/api/data-movie/${id}`)
      .then((res) => {
        let data = res.data;
        setPost({
          title: data.title,
          description: data.description,
          duration: data.duration,
          year: data.year,
          genre: data.genre,
          image_url: data.image_url,
          review: data.review,
          rating: data.rating,
        });
      });
  }, []);
  return (
    <Container>
      <Row>
        <Col span={6}>
          <img
            src={post.image_url}
            alt={post.title}
            width={230}
            height={200}
            style={{ overflow: "hidden", objectFit: "cover" }}
          />
        </Col>
        <Col span={18}>
          <h1>
            {post.title} ({post.year})
          </h1>
          <Divider />
          <Row>
            <Col span={2} className="detail-info">
              <p>Year</p> <p>Duration</p> <p>Genre</p> <p>Rating</p>
            </Col>
            <Col span={2} className="detail-info">
              <p>:</p> <p>:</p> <p>:</p> <p>:</p>
            </Col>
            <Col span={20} className="detail-info">
              <p>{post.year} </p>
              <p>{post.duration}</p>
              <p>{post.genre}</p>
              <p>{post.rating}</p>
            </Col>
          </Row>
        </Col>
      </Row>
      <Divider>Description</Divider>
      <p>{post.description}</p>
      <Divider>Review</Divider>
      <p>{post.review}</p>
    </Container>
  );
};
export default DetailMovie;
