import React, { useContext } from "react";
import { Card, Row, Col, Skeleton } from "antd";
import { DataContext } from "../../context/DataContext";
import { ClockCircleOutlined, StarFilled } from "@ant-design/icons";
import { Link } from "react-router-dom";
import Skeletons from "../utils/Skeleton";

const { Meta } = Card;
const CardMovie = ({ daftarDataMovie }) => {
  const { isLoading } = useContext(DataContext);

  return (
    <div>
      {isLoading ? (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          {/* <Col className="gutter-row" span={6}> */}
          <Skeletons />
          <Skeletons />
          <Skeletons />
          <Skeletons />
          {/* </Col> */}
        </Row>
      ) : (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          {daftarDataMovie.map((item, index) => {
            return (
              <Link to={`/movie/detail/${item.id}`}>
                <Col className="gutter-row" span={6}>
                  <Card
                    hoverable
                    style={{ width: 310 }}
                    cover={
                      <img
                        alt="example"
                        src={item.image_url}
                        style={{ height: 450 }}
                      />
                    }
                  >
                    <Meta
                      key={index}
                      title={item.title}
                      style={{ marginBottom: 10 }}
                    />
                    <div className="description">{item.description}</div>
                    <Row
                      gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}
                      style={{ marginBottom: "10px" }}
                    >
                      <Col className="gutter-row" span={18}>
                        <div>
                          <StarFilled
                            style={{ color: "gold", marginRight: "3px" }}
                          />
                          {item.rating}
                        </div>
                      </Col>
                      <Col className="gutter-row" span={6}>
                        <div>
                          <ClockCircleOutlined style={{ marginRight: "3px" }} />

                          {item.duration}
                        </div>
                      </Col>
                    </Row>
                    <button style={{ width: "100%", cursor: "pointer" }}>
                      View Details
                    </button>
                  </Card>
                </Col>
              </Link>
            );
          })}
        </Row>
      )}
    </div>
  );
};

export default CardMovie;
