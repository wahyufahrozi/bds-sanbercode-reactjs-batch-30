import React, { useContext } from "react";
import { Card, Row, Col, Skeleton } from "antd";
import { DataContext } from "../../context/DataContext";

import { Link } from "react-router-dom";
import Skeletons from "../utils/Skeleton";
const { Meta } = Card;

const CardGames = ({ daftarDataGame }) => {
  const { isLoading } = useContext(DataContext);

  return (
    <div>
      {isLoading ? (
        <>
          <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
            <Skeletons />
            <Skeletons />
            <Skeletons />
            <Skeletons />
          </Row>
        </>
      ) : (
        <Row gutter={{ xs: 8, sm: 16, md: 24, lg: 32 }}>
          {daftarDataGame.map((item, index) => {
            return (
              <Link to={`/game/detail/${item.id}`}>
                <Col className="gutter-row" span={6}>
                  <Card
                    hoverable
                    style={{ width: 310, marginBottom: "50px" }}
                    cover={
                      <img
                        alt="example"
                        src={item.image_url}
                        style={{ height: 450 }}
                      />
                    }
                  >
                    <Meta
                      key={index}
                      title={item.name}
                      style={{ marginBottom: 10 }}
                    />
                    <p>Genre : {item.genre}</p>
                    <p>Release : {item.release}</p>
                    <button style={{ width: "100%", cursor: "pointer" }}>
                      View Details
                    </button>
                  </Card>
                </Col>
              </Link>
            );
          })}
        </Row>
      )}
    </div>
  );
};

export default CardGames;
