import React, { useContext, useState } from "react";

import Container from "../container";
import { DataContext } from "../../context/DataContext";
import axios from "axios";
import Cookies from "js-cookie";
import { message, Radio, Row, Col } from "antd";

import { useHistory } from "react-router-dom";
const GamesForm = () => {
  const { input, setInput, currentId, setCurrentId, fetchDataGame } =
    useContext(DataContext);
  const [multiplayerCheck, setmultiplayerCheck] = useState(false);
  const [singleplayerCheck, setsingleplayerCheck] = useState(false);

  let history = useHistory();
  const successAdd = () => {
    message.success("Data berhasil ditambah!");
  };
  const successEdit = () => {
    message.success("Data Berhasil diubah");
  };
  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;

    setInput({ ...input, [name]: value });
    console.log(value);
  };
  // const onChange = (e) => {
  //   console.log("radio checked", e.target.value);
  //   setValue(e.target.value);
  // };
  const handleSubmit = (e) => {
    e.preventDefault();
    let {
      name,
      genre,
      image_url,
      release,
      platform,

      // singleplayerCheck,
      // multiplayerCheck,
    } = input;
    // const testData = {
    //   name,
    //   genre,
    //   image_url,
    //   release,
    //   platform,
    //   singlePlayer: singleplayerCheck,
    //   multiplayer: multiplayerCheck,
    // };
    // console.log(testData);
    if (currentId === null) {
      axios
        .post(
          `https://backendexample.sanbersy.com/api/data-game`,
          {
            name,
            genre,
            image_url,
            release,
            platform,
            singlePlayer: singleplayerCheck,
            multiplayer: multiplayerCheck,
          },
          { headers: { Authorization: "Bearer " + Cookies.get("token") } }
        )
        .then(() => {
          fetchDataGame();
          submitButton();
          successAdd();
          setInput({
            name: "",
            genre: "",
            image_url: "",
            release: "",
            platform: "",
            singlePlayer: singleplayerCheck,
            multiplayer: multiplayerCheck,
          });
        });
    } else {
      axios
        .put(
          `https://backendexample.sanbersy.com/api/data-game/${currentId}`,
          {
            name,
            genre,
            image_url,
            release,
            platform,
            singlePlayer: singleplayerCheck,
            multiplayer: multiplayerCheck,
          },
          {
            headers: { Authorization: "Bearer " + Cookies.get("token") },
          }
        )
        .then(() => {
          fetchDataGame();
          successEdit();
          submitButton();
          setInput({
            name: "",
            genre: "",
            image_url: "",
            release: "",
            platform: "",
            singlePlayer: singleplayerCheck,
            multiplayer: multiplayerCheck,
          });
        });
    }
  };
  function submitButton() {
    history.push("/admin/gamelist");
  }
  return (
    <Container>
      <form onSubmit={handleSubmit}>
        <label for="title">Name</label>
        <input
          type="text"
          id="name"
          name="name"
          required
          placeholder="name"
          value={input.name}
          onChange={handleChange}
        />
        <label for="genre">Genre</label>
        <input
          type="text"
          id="genre"
          name="genre"
          placeholder="Genre"
          required
          value={input.genre}
          onChange={handleChange}
        />
        <label for="platform">platform</label>
        <input
          type="text"
          id="platform"
          name="platform"
          placeholder="platform"
          required
          value={input.platform}
          onChange={handleChange}
        />
        <label for="genre">Image Url</label>
        <input
          type="text"
          id="image_url"
          name="image_url"
          required
          placeholder="Image Url"
          value={input.image_url}
          onChange={handleChange}
        />
        <label for="release">Release</label>
        <input
          type="number"
          id="release"
          name="release"
          placeholder="release"
          required
          min={1980}
          max={2021}
          value={input.release}
          onChange={handleChange}
        />
        <br />

        <Row gutter={16}>
          <Col span={8}>
            <label for="multiplayer">Multiplayer</label> <br />
            <Radio.Group>
              <Radio
                value={true}
                onChange={(e) => {
                  setmultiplayerCheck(e.target.value);
                }}
              >
                Yes
              </Radio>
              <Radio
                value={false}
                onChange={(e) => {
                  setmultiplayerCheck(e.target.value);
                }}
              >
                No
              </Radio>
            </Radio.Group>
          </Col>
          <Col span={16}>
            <label for="singleplayer">Singleplayer</label> <br />
            <Radio.Group>
              <Radio
                value={true}
                onChange={(e) => {
                  setsingleplayerCheck(e.target.value);
                }}
              >
                Yes
              </Radio>
              <Radio
                value={false}
                onChange={(e) => {
                  setsingleplayerCheck(e.target.value);
                }}
              >
                No
              </Radio>
            </Radio.Group>
          </Col>
        </Row>
        <br />
        <input type="submit" value="Submit" />
      </form>
    </Container>
  );
};
export default GamesForm;
