import axios from "axios";
import React, { useState, useContext } from "react";

import Container from "../container";
import Cookies from "js-cookie";
import { useHistory } from "react-router-dom";
import { message } from "antd";
import { DataContext } from "../../context/DataContext";
const ChangePassword = () => {
  const { setLoginStatus } = useContext(DataContext);
  let history = useHistory();
  const successEdit = () => {
    message.success("Password Berhasil diubah,Silahkan login kembali", 4);
  };
  const wrongPassword = () => {
    message.error("Password Tidak Cocok");
  };
  const [current_password, setCurrentPassword] = useState("");
  const [new_password, setNew_password] = useState("");
  const [new_confirm_password, setNew_confirm_password] = useState("");
  const handleSubmit = (e) => {
    e.preventDefault();
    // let { current_password, new_password, new_confirm_password } = input;
    if (new_password !== new_confirm_password) {
      wrongPassword();
    } else {
      axios
        .post(
          `https://backendexample.sanbersy.com/api/change-password`,
          {
            current_password,
            new_password,
            new_confirm_password,
          },
          { headers: { Authorization: "Bearer " + Cookies.get("token") } }
        )
        .then(() => {
          history.push("/signin");
          successEdit();
          setLoginStatus(false);
          Cookies.remove("user");
          Cookies.remove("email");
          Cookies.remove("token");
          history.push("/signin");
        });
    }
  };
  return (
    <Container>
      <form onSubmit={handleSubmit}>
        <label for="currentp">Current Password</label>
        <input
          type="password"
          id="current_password"
          name="current_password"
          placeholder="Current Password"
          required
          onChange={(e) => setCurrentPassword(e.target.value)}
        />
        <label for="new_password">New Password</label>
        <input
          type="password"
          id="new_password"
          name="new_password"
          placeholder="New Password"
          required
          onChange={(e) => setNew_password(e.target.value)}
        />
        <label for="genre">Password Confirm</label>
        <input
          type="password"
          id="pconfirm"
          name="pconfirm"
          required
          placeholder="Password Confirm"
          onChange={(e) => setNew_confirm_password(e.target.value)}
        />
        <input type="submit" value="Submit" />
      </form>
    </Container>
  );
};
export default ChangePassword;
