import React, { useContext } from "react";
import Container from "../container";
import { DataContext } from "../../context/DataContext";
import axios from "axios";
import Cookies from "js-cookie";
import { message } from "antd";

import { useHistory } from "react-router-dom";

const MoviesForm = () => {
  const { input, setInput, currentId, setCurrentId, fetchDataMovie } =
    useContext(DataContext);
  let history = useHistory();
  const successAdd = () => {
    message.success("Data berhasil ditambah!");
  };
  const successEdit = () => {
    message.success("Data Berhasil diubah");
  };
  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
  };
  const handleSubmit = (e) => {
    e.preventDefault();
    let {
      title,
      description,
      genre,
      rating,
      review,
      year,
      image_url,
      duration,
    } = input;
    if (currentId === null) {
      axios
        .post(
          `https://backendexample.sanbersy.com/api/data-movie`,
          {
            title,
            description,
            genre,
            rating,
            review,
            year,
            image_url,
            duration,
          },
          { headers: { Authorization: "Bearer " + Cookies.get("token") } }
        )
        .then(() => {
          fetchDataMovie();
          submitButton();
          successAdd();
          setInput({
            title: "",
            description: "",
            genre: "",
            rating: "",
            review: "",
            year: "",
            image_url: "",
            duration: "",
          });
        });
    } else {
      axios
        .put(
          `https://backendexample.sanbersy.com/api/data-movie/${currentId}`,
          {
            title,
            description,
            genre,
            rating,
            review,
            year,
            image_url,
            duration,
          },
          { headers: { Authorization: "Bearer " + Cookies.get("token") } }
        )
        .then(() => {
          fetchDataMovie();
          submitButton();
          successEdit();
          setCurrentId(null);
          setInput({
            title: "",
            description: "",
            genre: "",
            rating: "",
            review: "",
            year: "",
            image_url: "",
            duration: "",
          });
        });
    }
  };
  const submitButton = () => {
    history.push("/admin/movielist");
  };

  return (
    <Container>
      <form onSubmit={handleSubmit}>
        <label for="title">Title</label>
        <input
          type="text"
          id="title"
          name="title"
          required
          placeholder="title"
          value={input.title}
          onChange={handleChange}
        />

        <label for="description">Description</label>
        <textarea
          type="text"
          required
          id="description"
          name="description"
          placeholder="Description"
          value={input.description}
          onChange={handleChange}
        ></textarea>
        <label for="duration">Duration</label>
        <input
          type="number"
          id="duration"
          required
          name="duration"
          placeholder="Duration"
          value={input.duration}
          onChange={handleChange}
        />
        <label for="genre">Genre</label>
        <input
          type="text"
          id="genre"
          name="genre"
          placeholder="Genre"
          required
          value={input.genre}
          onChange={handleChange}
        />
        <label for="genre">Image Url</label>
        <input
          type="text"
          id="image_url"
          name="image_url"
          required
          placeholder="Image Url"
          value={input.image_url}
          onChange={handleChange}
        />
        <label for="rating">Rating</label>
        <input
          type="number"
          id="rating"
          required
          name="rating"
          placeholder="Rating"
          value={input.rating}
          onChange={handleChange}
          min={0}
          max={10}
        />
        <label for="review">Review</label>
        <input
          type="text"
          id="review"
          required
          name="review"
          placeholder="Review"
          value={input.review}
          onChange={handleChange}
        />
        <label for="year">Year</label>
        <input
          type="number"
          id="year"
          name="year"
          placeholder="year"
          required
          min={1980}
          max={2021}
          value={input.year}
          onChange={handleChange}
        />
        <input type="submit" value="Submit" />
      </form>
    </Container>
  );
};
export default MoviesForm;
