import "./App.css";
import Routes from "./components/Navbar/Routes";

function App() {
  return (
    <>
      <div>
        <Routes />
      </div>
    </>
  );
}

export default App;
