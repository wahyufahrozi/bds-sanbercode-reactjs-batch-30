import React, { useContext } from "react";
import { GameContext } from "../../context/GameContext";

const MainContent = () => {
  const {
    daftarGame,
    searchInput,

    filteredResults,
  } = useContext(GameContext);
  const formatRupiah = (money) => {
    return new Intl.NumberFormat("id-ID", {
      style: "currency",
      currency: "IDR",
      minimumFractionDigits: 0,
    }).format(money);
  };

  return (
    <div className="container">
      <div className="content">
        {/* <Search
          placeholder="input search text"
          allowClear
          enterButton="Search"
          size="medium"
          className="searcbox"
          onChange={(e) => searchItems(e.target.value)}
        /> */}
        <h1 style={{ textAlign: "center" }}>Popular Mobile Apps</h1>
        {searchInput.length > 1
          ? filteredResults.map((item, index) => {
              return (
                <>
                  <h2 key={index}>{item.name}</h2>
                  <h4>Release Year :{item.release_year}</h4>
                  <img
                    src={item.image_url}
                    width={1000}
                    height={600}
                    alt="Gambar"
                  />
                  <div>
                    <strong>
                      Price:{" "}
                      {item.price !== 0 ? formatRupiah(item.price) : "Free"}
                    </strong>

                    <br />
                    <strong>Rating: {item.rating}</strong>
                    <br />
                    <strong>Size: {item.byte}</strong>
                    <br />
                    <strong>
                      Platform: {item.android} {item.ios}
                    </strong>
                    <br />
                    <hr />
                  </div>
                </>
              );
            })
          : daftarGame.map((item, index) => {
              return (
                <>
                  <h2 key={index}>{item.name}</h2>
                  <h4>Release Year :{item.release_year}</h4>
                  <img
                    src={item.image_url}
                    width={1000}
                    height={600}
                    alt="Gambar"
                  />
                  <div>
                    <strong>
                      Price:{" "}
                      {item.price !== 0 ? formatRupiah(item.price) : "Free"}
                    </strong>

                    <br />
                    <strong>Rating: {item.rating}</strong>
                    <br />
                    <strong>Size: {item.byte}</strong>
                    <br />
                    <strong>
                      Platform: {item.android}
                      {item.android && item.ios ? " & " : null}
                      {item.ios}
                      {/* {item.android > 0 ? "Android" : null}
                      {item.ios > 0 ? "IOS" : null} */}
                    </strong>
                    <br />
                    <br />
                    <div className="description">
                      <b>Description</b> : {item.description}
                    </div>

                    <hr />
                  </div>
                </>
              );
            })}
      </div>
    </div>
  );
};

export default MainContent;
