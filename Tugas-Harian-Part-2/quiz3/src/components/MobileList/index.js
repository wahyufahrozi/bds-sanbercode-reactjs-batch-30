import React from "react";
import { Link } from "react-router-dom";
import { useHistory } from "react-router-dom";
import { useContext } from "react/cjs/react.development";
import { GameContext } from "../../context/GameContext";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { message, Table, Space, Tooltip, Button } from "antd";
import { PlusOutlined } from "@ant-design/icons";
import axios from "axios";

const MobileList = () => {
  let history = useHistory();
  const { daftarGame, setCurrentId, setInput, fetchData } =
    useContext(GameContext);
  const success = () => {
    message.success("Data Terhapus");
  };
  const textDelete = <span>Delete</span>;
  const textEdit = <span>Edit</span>;
  const handleDelete = (record) => {
    let idGame = record.id;
    axios
      .delete(`http://backendexample.sanbercloud.com/api/mobile-apps/${idGame}`)
      .then(() => {
        fetchData();
        success();
      });
  };

  const handleEdit = (record) => {
    let idGame = record.id;
    axios
      .get(`http://backendexample.sanbercloud.com/api/mobile-apps/${idGame}`)
      .then((result) => {
        let data = result.data;
        setInput({
          name: data.name,
          category: data.category,
          description: data.description,
          release_year: data.release_year,
          size: data.size,
          price: data.price,
          rating: data.rating,
          image_url: data.image_url,
          is_android_app: data.is_android_app,
          is_ios_app: data.is_ios_app,
        });
        setCurrentId(data.id);
        editButton();
      });
  };
  function editButton() {
    history.push("/mobilelist/edit-data");
  }

  const column = [
    {
      title: "No",
      key: "index",
      render: (text, record, index) => index + 1,
    },

    {
      title: "Name",
      dataIndex: "name",
      key: "key",
    },
    {
      title: "Category",
      dataIndex: "category",
      key: "key",
    },
    {
      title: "Description",
      dataIndex: "description",
      key: "key",
      ellipsis: {
        showTitle: false,
      },
      render: (description) => (
        <Tooltip placement="topLeft" title={description}>
          {description}
        </Tooltip>
      ),
    },

    {
      title: "Release year",
      dataIndex: "release_year",
      key: "key",
    },
    {
      title: "size",
      dataIndex: "byte",
      key: "key",
    },
    {
      title: "Price",
      dataIndex: "price",
      key: "key",
    },
    {
      title: "Rating",
      dataIndex: "rating",
      key: "key",
    },
    {
      title: "Platform",
      key: "key",
      render: (text, record) => [record.ios, record.android],
    },
    {
      title: "Aksi",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <Tooltip placement="top" title={textEdit} color="green">
            <button className="edit-button" onClick={() => handleEdit(record)}>
              <EditOutlined />
            </button>
          </Tooltip>
          <Tooltip placement="top" title={textDelete} color="red">
            <button
              className="delete-button"
              onClick={() => handleDelete(record)}
            >
              <DeleteOutlined />
            </button>
          </Tooltip>
        </Space>
      ),
    },
  ];
  return (
    <div className="container">
      <div className="content">
        <Button type="primary" icon={<PlusOutlined />}>
          <Link to="/create-data" style={{ color: "white" }}>
            Create Data
          </Link>
        </Button>

        <Table dataSource={daftarGame} columns={column} />
      </div>
    </div>
  );
};

export default MobileList;
