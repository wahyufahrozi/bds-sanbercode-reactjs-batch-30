import { BrowserRouter as Router, Route, Switch } from "react-router-dom";
import Navbar from "./index";
import MainContent from "../MainContent";
import MobileList from "../MobileList";
import About from "../About";
import Form from "../Form";
import EditForm from "../Form";
import Footer from "../Footer";
const Routes = () => {
  return (
    <>
      <Router>
        <Navbar />
        <Switch>
          <Route path="/" exact component={MainContent} />
          <Route path="/mobilelist" exact component={MobileList} />
          <Route path="/about" exact component={About} />
          <Route path="/create-data" exact component={Form} />
          <Route path="/mobilelist/edit-data" exact component={EditForm} />
        </Switch>
        <Footer />
      </Router>
    </>
  );
};

export default Routes;
