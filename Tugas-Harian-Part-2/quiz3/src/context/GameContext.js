import React, { createContext, useState, useEffect } from "react";
import axios from "axios";

export const GameContext = createContext();

export const GameProvider = (props) => {
  const [daftarGame, setDaftarGame] = useState([]);
  const [input, setInput] = useState("");
  const [checkAndroid, setCheckAndroid] = useState(false);
  const [checkIos, setCheckIos] = useState(false);
  const [searchInput, setSearchInput] = useState("");
  const [filteredResults, setFilteredResults] = useState([]);
  const [currentId, setCurrentId] = useState(null);

  const niceBytes = (x) => {
    if (x < 1000) {
      return `${x} MB`;
    } else {
      let result = x / 1000;
      return `${Math.floor(result)} GB`;
    }
  };
  const Platformandroid = (action) => {
    if (action === 1) {
      return "Android";
    }
  };
  const Platformios = (action) => {
    if (action === 1) {
      return "Ios";
    }
  };
  const searchItems = (search) => {
    setSearchInput(search);
    if (searchInput !== "") {
      const filteredData = daftarGame.filter((item) => {
        return Object.values(item)
          .join("")
          .toLocaleLowerCase()
          .includes(searchInput.toLowerCase());
      });
      setFilteredResults(filteredData);
    } else {
      setFilteredResults(daftarGame);
    }
  };
  const fetchData = async () => {
    const res = await axios.get(
      `http://backendexample.sanbercloud.com/api/mobile-apps`
    );
    setDaftarGame(
      res.data.map((item) => {
        // let Rp = formatRupiah(item.price);
        // console.log(item.size);
        let android = Platformandroid(item.is_android_app);
        let ios = Platformios(item.is_ios_app);
        let byte = niceBytes(item.size);
        // let gabungan = [android, ios];

        return {
          id: item.id,
          name: item.name,
          description: item.description,
          category: item.category,
          byte,
          price: item.price,
          rating: item.rating,
          image_url: item.image_url,
          release_year: item.release_year,
          android,
          //   is_android_app: item.is_android_app,
          ios,
        };
      })
    );
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <GameContext.Provider
      value={{
        daftarGame,
        setDaftarGame,
        input,
        setInput,
        currentId,
        setCurrentId,
        searchInput,
        setSearchInput,
        filteredResults,
        setFilteredResults,
        searchItems,
        checkAndroid,
        setCheckAndroid,
        checkIos,
        setCheckIos,
        fetchData,
      }}
    >
      {props.children}
    </GameContext.Provider>
  );
};
