import React, { useState } from "react";
import "./tugas10.css";
import listBuah from "./listBuah";
import Nodata from "./no-data.jpg";
const Tugas11 = () => {
  const [buahan, setBuah] = useState(listBuah);
  const [input, setInput] = useState({
    nama: "",
    hargaTotal: "",
    beratTotal: "",
  });
  const [currentIndex, setCurrentIndex] = useState(null);
  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    let { nama, hargaTotal, beratTotal } = input;
    let newData = buahan;
    if (currentIndex === null) {
      newData = [...newData, { nama, hargaTotal, beratTotal }];
    } else {
      newData[currentIndex] = { nama, hargaTotal, beratTotal };
    }
    setBuah(newData);
    setInput({
      nama: "",
      hargaTotal: "",
      beratTotal: "",
    });
    setCurrentIndex(null);
  };

  const handleDelete = (e) => {
    let deleteValue = parseInt(e.target.value);
    let deletedData = buahan[deleteValue];
    let newData = buahan.filter((e) => {
      return e !== deletedData;
    });
    setBuah(newData);
  };

  const handleEdit = (e) => {
    let editValue = parseInt(e.target.value);
    let editedData = buahan[editValue];
    setInput({
      nama: editedData.nama,
      hargaTotal: editedData.hargaTotal,
      beratTotal: editedData.beratTotal,
    });
    setCurrentIndex(editValue);
  };

  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Daftar Harga Buah</h1>
      {buahan.length > 0 ? (
        <table className="table-content-11">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Harga Total</th>
              <th>Berat Total</th>
              <th>Harga per kg</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {buahan.map((item, index) => {
              let beratTotalKg = item.beratTotal * 0.001;
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.nama} </td>
                  <td> {item.hargaTotal} </td>
                  <td>{beratTotalKg} Kg </td>
                  <td>{Math.ceil(item.hargaTotal / beratTotalKg)}</td>
                  <td>
                    <button
                      className="edit-button"
                      onClick={handleEdit}
                      value={index}
                    >
                      Edit
                    </button>
                    <button
                      className="delete-button"
                      onClick={handleDelete}
                      value={index}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <div className="image-data">
          <img src={Nodata} alt="No data" />
        </div>
      )}
      <h1 style={{ textAlign: "center" }}>Form Daftar Harga Buah</h1>
      <div className="form-buah">
        <form onSubmit={handleSubmit}>
          <div className="box">
            <label>Nama :</label>
            <input
              type="text"
              name="nama"
              value={input.nama}
              onChange={handleChange}
              required
            />
          </div>
          <br />
          <div className="box">
            <label>Harga Total :</label>

            <input
              type="number"
              name="hargaTotal"
              value={input.hargaTotal}
              onChange={handleChange}
              required
            />
          </div>
          <br />
          <div className="box">
            <label>Berat Total(dalam gram) :</label>
            <input
              type="number"
              name="beratTotal"
              value={input.beratTotal}
              onChange={handleChange}
              min={2000}
              required
            />
          </div>
          <br />
          <input type="submit" id="button-submit" />
        </form>
      </div>
    </div>
  );
};
export default Tugas11;
