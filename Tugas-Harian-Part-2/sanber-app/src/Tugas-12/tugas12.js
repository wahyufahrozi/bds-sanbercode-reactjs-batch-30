import React, { useEffect, useState } from "react";
import axios from "axios";
import "./tugas12.css";
import Nodata from "./no-data.jpg";

const Tugas12 = () => {
  const [daftarMahasiswa, setDaftarMahasiswa] = useState([]);
  const [inputName, setInputName] = useState("");
  const [currentId, setCurrentId] = useState(null);

  const fetchData = async () => {
    const result = await axios.get(
      `http://backendexample.sanbercloud.com/api/student-scores`
    );
    setDaftarMahasiswa(
      result.data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          course: item.course,
          score: item.score,
        };
      })
    );
  };
  useEffect(() => {
    fetchData();
  }, []);

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInputName({ ...inputName, [name]: value });
    console.log(value);
  };

  const postData = () => {
    let { name, course, score } = inputName;
    axios
      .post(`http://backendexample.sanbercloud.com/api/student-scores`, {
        name,
        course,
        score,
      })
      .then((res) => {
        fetchData();
        // let data = res.data;
        // setDaftarMahasiswa([
        //   ...daftarMahasiswa,
        //   {
        //     id: data.id,
        //     name: data.name,
        //     course: data.course,
        //     score: data.score,
        //   },
        // ]);
      });
  };
  const putData = () => {
    let { name, course, score } = inputName;
    axios
      .put(
        ` http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
        {
          name,
          course,
          score,
        }
      )
      .then((res) => {
        fetchData();
      });
  };

  const handleSubmit = (e) => {
    e.preventDefault();
    // console.log(inputName.nama);
    if (currentId === null) {
      postData();
      setInputName({
        name: "",
        course: "",
        score: "",
      });
    } else {
      putData();
      setInputName({
        name: "",
        course: "",
        score: "",
      });
      setCurrentId(null);
    }

    setInputName({
      name: "",
      course: "",
      score: "",
    });
    setCurrentId(null);
  };
  const handleEdit = (e) => {
    let idMahasiswa = e.target.value;
    console.log(idMahasiswa);
    axios
      .get(
        `http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then((result) => {
        let data = result.data;
        console.log(data);
        setInputName({
          name: data.name,
          course: data.course,
          score: data.score,
        });
        setCurrentId(data.id);
      });
  };
  const handleDelete = (e) => {
    let idMahasiswa = parseInt(e.target.value);
    // console.log(idMahasiswa);
    axios
      .delete(
        ` http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then(() => {
        let newDaftarMahasiswa = daftarMahasiswa.filter((item) => {
          return item.id !== idMahasiswa;
        });
        setDaftarMahasiswa(newDaftarMahasiswa);
      });
  };

  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Daftar Mahasiswa</h1>
      {daftarMahasiswa.length > 0 ? (
        <table className="table-content-12">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Mata Kuliah</th>
              <th>Nilai</th>
              <th>Indeks Nilai</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {daftarMahasiswa.map((item, index) => {
              let indeksNilai = "";
              if (item.score >= 80) {
                indeksNilai = "A";
              } else if (item.score < 80 && item.score >= 70) {
                indeksNilai = "B";
              } else if (item.score < 70 && item.score >= 60) {
                indeksNilai = "C";
              } else if (item.score < 60 && item.score >= 50) {
                indeksNilai = "D";
              } else {
                indeksNilai = "E";
              }
              //   console.log(indeksNilai);
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.name} </td>
                  <td> {item.course} </td>
                  <td>{item.score} </td>
                  <td>{indeksNilai}</td>
                  <td>
                    <button
                      className="edit-button"
                      onClick={handleEdit}
                      value={item.id}
                    >
                      Edit
                    </button>
                    <button
                      className="delete-button"
                      onClick={handleDelete}
                      value={item.id}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <div className="image-data">
          <img src={Nodata} alt="No data" />
        </div>
      )}
      <h1 style={{ textAlign: "center" }}>Form Daftar Mahasiswa</h1>
      <div className="form-buah">
        <form onSubmit={handleSubmit}>
          <div className="box">
            <label>Nama :</label>
            <input
              type="text"
              name="name"
              value={inputName.name}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Mata Kuliah :</label>

            <input
              type="text"
              name="course"
              value={inputName.course}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Nilai :</label>
            <input
              type="number"
              style={{ width: "19%" }}
              name="score"
              min={0}
              max={100}
              required
              onChange={handleChange}
              value={inputName.score}
            />
          </div>
          <br />
          <input type="submit" id="button-submit" />
        </form>
      </div>
    </div>
  );
};
export default Tugas12;
