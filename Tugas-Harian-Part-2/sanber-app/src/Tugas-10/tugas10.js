import React, { useState, useEffect } from "react";
import "./tugas10.css";

const Tugas10 = () => {
  const [clock, setClock] = useState();
  const [countDown, setCountDown] = useState(20);
  useEffect(() => {
    setInterval(() => {
      const date = new Date();
      setClock(date.toLocaleTimeString());
    }, 1000);
    if (countDown > 0) {
      setTimeout(() => setCountDown(countDown - 1), 1000);
    }
  }, [countDown]);

  return countDown !== 0 ? (
    <div className="container">
      <h1>Now AT - {clock}</h1>
      <h3>Countdown : {countDown}</h3>
    </div>
  ) : null;
};

export default Tugas10;
