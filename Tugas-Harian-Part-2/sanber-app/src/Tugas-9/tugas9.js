import React from "react";
import logo from "../assets/img/logo.png";
import MainContent from "./MainContent";
const Tugas9 = () => {
  return (
    <div className="container-app">
      <div className="logo-app">
        <img src={logo} alt="Logo" />
      </div>
      <div className="header">
        <h2>THINGS TO DO</h2>
        <p>During bootcamp in sanbercode</p>
      </div>
      <MainContent
        id="BelajarGit"
        name="belajargit"
        value="belajargit"
        label="Belajar Git & CLI"
      />
      <MainContent
        id="BelajarHTML"
        name="BelajarHTML"
        value="BelajarHTML"
        label="Belajar HTML & CSS"
      />
      <MainContent
        id="BelajarJavascript"
        name="BelajarJavascript"
        value="BelajarJavascript"
        label="Belajar Javascript"
      />
      <MainContent
        id="BelajarReactjs"
        name="BelajarReactjs"
        value="BelajarReactjs"
        label="Belajar ReactJs Dasar"
      />
      <MainContent
        id="BelajarJSad"
        name="BelajarJSad"
        value="BelajarJSad"
        label="Belajar ReactJs Advance"
      />
      <button className="button-submit">SEND</button>
    </div>
  );
};
export default Tugas9;
