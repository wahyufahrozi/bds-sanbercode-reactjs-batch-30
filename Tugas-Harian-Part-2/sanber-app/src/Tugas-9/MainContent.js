import React from "react";
import "./maincontent.css";
export default function MainContent({ id, name, value, label }) {
  return (
    <div className="main-content">
      <label>
        <input type="checkbox" id={id} name={name} value={value} />
        {label}
      </label>
    </div>
  );
}
