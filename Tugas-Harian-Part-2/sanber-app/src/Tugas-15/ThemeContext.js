import React, { createContext, useState } from "react";

export const ThemeContext = createContext();

export const ThemeProvider = (props) => {
  const [style, setStyle] = useState("header-nav");
  const [text, setText] = useState("Change Nav to Dark Theme");

  return (
    <ThemeContext.Provider value={{ style, setStyle, text, setText }}>
      {props.children}
    </ThemeContext.Provider>
  );
};
