import axios from "axios";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { MahasiswaContexts } from "./mahasiswaContext";
import { message } from "antd";

const MahasiswaForm = () => {
  const history15 = useHistory();

  const { input, setInput, currentId, setCurrentId, fetchData } =
    useContext(MahasiswaContexts);

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
    console.log(value);
  };
  const successAdd = () => {
    message.success("Data berhasil ditambah!");
  };
  const successEdit = () => {
    message.success("Data berhasil diubah!");
  };
  const handleSubmit = (e) => {
    let { name, score, course } = input;
    e.preventDefault();
    // console.log("asdasd", currentId);
    if (currentId === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/student-scores`, {
          name,
          course,
          score,
        })
        .then((res) => {
          fetchData();
          submitButton();
          successAdd();
        });
      setInput({
        name: "",
        score: "",
        course: "",
      });
    } else {
      axios
        .put(
          ` http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
          {
            name,
            course,
            score,
          }
        )
        .then((res) => {
          fetchData();
          submitButton();
          successEdit();
        });

      setCurrentId(null);
      setInput({
        name: "",
        score: "",
        course: "",
      });
    }
  };
  function submitButton() {
    history15.push("/tugas15");
  }
  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Form Daftar Mahasiswa</h1>
      <div className="form-mahasiswa">
        <form onSubmit={handleSubmit}>
          <div className="box">
            <label>Nama :</label>
            <input
              type="text"
              name="name"
              value={input.name}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Mata Kuliah :</label>

            <input
              type="text"
              name="course"
              value={input.course}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Nilai :</label>
            <input
              type="number"
              style={{ width: "19%" }}
              name="score"
              min={0}
              max={100}
              required
              onChange={handleChange}
              value={input.score}
            />
          </div>
          <br />
          <input type="submit" id="button-submit" />
          <p
            onClick={() => submitButton()}
            style={{
              padding: "20px",
              cursor: "pointer",
              textDecoration: "underline",
              color: "blue",
            }}
          >
            Back To List Mahasiswa
          </p>
        </form>
      </div>
    </div>
  );
};

export default MahasiswaForm;
