import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
export const MahasiswaContexts = createContext();

export const MahasiswaProviders = (props) => {
  const [daftarMahasiswa, setDaftarMahasiswa] = useState([]);
  const [input, setInput] = useState("");
  const [currentId, setCurrentId] = useState(null);

  const getScore = (score) => {
    if (score >= 80) {
      return "A";
    } else if (score < 80 && score >= 70) {
      return "B";
    } else if (score < 70 && score >= 60) {
      return "C";
    } else if (score < 60 && score >= 50) {
      return "D";
    } else {
      return "E";
    }
  };
  const fetchData = async () => {
    const result = await axios.get(
      `http://backendexample.sanbercloud.com/api/student-scores`
    );

    setDaftarMahasiswa(
      result.data.map((item) => {
        let indexScore = getScore(item.score);
        return {
          id: item.id,
          name: item.name,
          course: item.course,
          score: item.score,
          indexScore: indexScore,
        };
      })
    );
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <MahasiswaContexts.Provider
      value={{
        daftarMahasiswa,
        setDaftarMahasiswa,
        input,
        setInput,
        currentId,
        setCurrentId,
        fetchData,
      }}
    >
      {props.children}
    </MahasiswaContexts.Provider>
  );
};
