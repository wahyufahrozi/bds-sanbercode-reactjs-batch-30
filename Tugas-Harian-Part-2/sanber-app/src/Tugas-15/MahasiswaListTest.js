import axios from "axios";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { MahasiswaContexts } from "./mahasiswaContext";
import Nodata from "./no-data.jpg";
import { DeleteOutlined, EditOutlined } from "@ant-design/icons";
import { message, Button, Table, Space } from "antd";

import "./tugas15.css";

const MahasiswaList = () => {
  let history = useHistory();
  const { daftarMahasiswa, setCurrentId, setInput, fetchData } =
    useContext(MahasiswaContexts);

  const success = () => {
    message.success("Data Terhapus");
  };
  const handleDelete = (record) => {
    let idMahasiswa = record.id;
    console.log(idMahasiswa);
    axios
      .delete(
        ` http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then(() => {
        fetchData();
        success();
      });
  };

  const handleEdit = (record) => {
    let idMahasiswa = record.id;
    axios
      .get(
        `http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then((result) => {
        let data = result.data;
        console.log(data);
        setInput({
          name: data.name,
          course: data.course,
          score: data.score,
        });
        setCurrentId(data.id);
        CreateButtons();
      });
  };
  function CreateButtons() {
    history.push("/tugas15/create");
  }

  const column = [
    {
      title: "No",
      key: "index",
      render: (text, record, index) => index + 1,
    },

    {
      title: "Nama",
      dataIndex: "name",
      key: "key",
    },
    {
      title: "Mata Kuliah",
      dataIndex: "course",
      key: "key",
    },
    {
      title: "Nilai",
      dataIndex: "score",
      key: "key",
    },
    {
      title: "Indeks Nilai",
      dataIndex: "indexScore",
      key: "key",
    },
    {
      title: "Aksi",
      key: "action",
      render: (text, record) => (
        <Space size="middle">
          <button id="edit-button" onClick={() => handleEdit(record)}>
            <EditOutlined />
          </button>
          <button id="delete-button" onClick={() => handleDelete(record)}>
            <DeleteOutlined />
          </button>
        </Space>
      ),
    },
  ];
  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Daftar Mahasiswa</h1>
      <Button
        type="primary"
        onClick={CreateButtons}
        style={{ cursor: "pointer" }}
      >
        Buat Data Nilai Mahasiswa Baru
      </Button>
      {daftarMahasiswa.length > 0 ? (
        //   console.log(indeksNilai);
        <Table dataSource={daftarMahasiswa} columns={column} />
      ) : (
        <div className="image-data">
          <img src={Nodata} alt="No data" />
        </div>
      )}
    </div>
  );
};

export default MahasiswaList;
