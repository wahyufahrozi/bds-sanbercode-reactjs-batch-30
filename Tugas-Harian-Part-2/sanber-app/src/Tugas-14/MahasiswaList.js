import axios from "axios";
import React, { useContext } from "react";
import { useHistory } from "react-router-dom";
import { MahasiswaContext } from "./mahasiswaContext";
import Nodata from "./no-data.jpg";
import "./tugas14.css";

const MahasiswaList = () => {
  let history = useHistory();
  const { daftarMahasiswa, setCurrentId, setInput, fetchData } =
    useContext(MahasiswaContext);

  const handleDelete = (e) => {
    let idMahasiswa = parseInt(e.target.value);
    // console.log(idMahasiswa);
    axios
      .delete(
        ` http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then(() => {
        fetchData();
      });
  };

  const handleEdit = (e) => {
    let idMahasiswa = parseInt(e.target.value);
    axios
      .get(
        `http://backendexample.sanbercloud.com/api/student-scores/${idMahasiswa}`
      )
      .then((result) => {
        let data = result.data;
        console.log(data);
        setInput({
          name: data.name,
          course: data.course,
          score: data.score,
        });
        setCurrentId(data.id);
        CreateButton();
      });
  };
  function CreateButton() {
    history.push("/tugas14/create");
  }

  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Daftar Mahasiswa</h1>
      <button onClick={CreateButton}>Buat Data Nilai Mahasiswa Baru</button>
      {daftarMahasiswa.length > 0 ? (
        <table className="table-content">
          <thead>
            <tr>
              <th>No</th>
              <th>Nama</th>
              <th>Mata Kuliah</th>
              <th>Nilai</th>
              <th>Indeks Nilai</th>
              <th>Aksi</th>
            </tr>
          </thead>
          <tbody>
            {daftarMahasiswa.map((item, index) => {
              let indeksNilai = "";
              if (item.score >= 80) {
                indeksNilai = "A";
              } else if (item.score < 80 && item.score >= 70) {
                indeksNilai = "B";
              } else if (item.score < 70 && item.score >= 60) {
                indeksNilai = "C";
              } else if (item.score < 60 && item.score >= 50) {
                indeksNilai = "D";
              } else {
                indeksNilai = "E";
              }
              //   console.log(indeksNilai);
              return (
                <tr key={index}>
                  <td>{index + 1}</td>
                  <td>{item.name} </td>
                  <td> {item.course} </td>
                  <td>{item.score} </td>
                  <td>{indeksNilai}</td>
                  <td>
                    <button
                      className="edit-button"
                      onClick={handleEdit}
                      value={item.id}
                    >
                      Edit
                    </button>
                    <button
                      className="delete-button"
                      onClick={handleDelete}
                      value={item.id}
                    >
                      Delete
                    </button>
                  </td>
                </tr>
              );
            })}
          </tbody>
        </table>
      ) : (
        <div className="image-data">
          <img src={Nodata} alt="No data" />
        </div>
      )}
    </div>
  );
};

export default MahasiswaList;
