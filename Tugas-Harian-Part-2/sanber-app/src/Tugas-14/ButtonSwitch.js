import React, { useContext } from "react";
import { ThemeContext } from "./ThemeContext";
import { Switch } from "antd";
const ButtonSwitch = () => {
  const { style, setStyle } = useContext(ThemeContext);
  return (
    <>
      <Switch
        checkedChildren="Dark"
        unCheckedChildren="Light"
        className="button-nav"
        onClick={() => {
          setStyle(style === "header-nav" ? "header-nav-dark" : "header-nav");
          // setText(style === "header-nav" ? "Dark" : "Ligth");
        }}
      />
    </>
  );
};

export default ButtonSwitch;
