import React from "react";

import { BrowserRouter as Router, Route, Switch } from "react-router-dom";

import Tugas9 from "../Tugas-9/tugas9";
import Tugas10 from "../Tugas-10/tugas10";
import Tugas11 from "../Tugas-11";
import Tugas12 from "../Tugas-12/tugas12";
import Tugas13 from "../Tugas-13/mahasiswa";
import Tugas14 from "./Mahasiswa";
import Tugas15 from "../Tugas-15/Mahasiswa";
import MahasiswaForm14 from "./MahasiswaForm";
import MahasiswaForm15 from "../Tugas-15/MahasiswaForm";

import Nav from "./Nav";
import "./tugas14.css";

const Routes = () => {
  return (
    <>
      <Router>
        <div>
          <Nav />
          <Switch>
            <Route path="/" exact component={Tugas9} />
            <Route path="/tugas10" exact component={Tugas10} />
            <Route path="/tugas11" exact component={Tugas11} />
            <Route path="/tugas12" exact component={Tugas12} />
            <Route path="/tugas13" exact component={Tugas13} />
            <Route path="/tugas14" exact component={Tugas14} />
            <Route path="/tugas15" exact component={Tugas15} />
            <Route path="/tugas14/create" exact component={MahasiswaForm14} />
            <Route path="/tugas15/create" exact component={MahasiswaForm15} />
          </Switch>
        </div>
      </Router>
    </>
  );
};

export default Routes;
