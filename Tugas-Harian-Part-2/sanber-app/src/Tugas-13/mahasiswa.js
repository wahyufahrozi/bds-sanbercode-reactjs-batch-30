import React from "react";
import { MahasiswaProvider } from "./mahasiswaContext";
import MahasiswaForm from "./MahasiswaForm";
import MahasiswaList from "./MahasiswaList";
import "./tugas13.css";

const Mahasiswa = () => {
  return (
    <MahasiswaProvider>
      <MahasiswaList />
      <MahasiswaForm />
    </MahasiswaProvider>
  );
};

export default Mahasiswa;
