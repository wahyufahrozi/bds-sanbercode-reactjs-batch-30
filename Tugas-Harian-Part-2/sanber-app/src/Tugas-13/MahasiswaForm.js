import axios from "axios";
import React, { useContext } from "react";
import { MahasiswaContext } from "./mahasiswaContext";
const MahasiswaForm = () => {
  const { input, setInput, currentId, setCurrentId, fetchData } =
    useContext(MahasiswaContext);

  const handleChange = (e) => {
    let value = e.target.value;
    let name = e.target.name;
    setInput({ ...input, [name]: value });
    console.log(value);
  };

  const handleSubmit = (e) => {
    let { name, score, course } = input;
    e.preventDefault();
    console.log("asdasd", currentId);
    if (currentId === null) {
      axios
        .post(`http://backendexample.sanbercloud.com/api/student-scores`, {
          name,
          course,
          score,
        })
        .then((res) => {
          fetchData();
        });
      setInput({
        name: "",
        score: "",
        course: "",
      });
    } else {
      axios
        .put(
          ` http://backendexample.sanbercloud.com/api/student-scores/${currentId}`,
          {
            name,
            course,
            score,
          }
        )
        .then((res) => {
          fetchData();
        });

      setCurrentId(null);
      setInput({
        name: "",
        score: "",
        course: "",
      });
    }
  };

  return (
    <div className="container">
      <h1 style={{ textAlign: "center" }}>Form Daftar Mahasiswa</h1>
      <div className="form-mahasiswa">
        <form onSubmit={handleSubmit}>
          <div className="box">
            <label>Nama :</label>
            <input
              type="text"
              name="name"
              value={input.name}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Mata Kuliah :</label>

            <input
              type="text"
              name="course"
              value={input.course}
              required
              onChange={handleChange}
            />
          </div>
          <br />
          <div className="box">
            <label>Nilai :</label>
            <input
              type="number"
              style={{ width: "19%" }}
              name="score"
              min={0}
              max={100}
              required
              onChange={handleChange}
              value={input.score}
            />
          </div>
          <br />
          <input type="submit" id="button-submit" />
        </form>
      </div>
    </div>
  );
};

export default MahasiswaForm;
