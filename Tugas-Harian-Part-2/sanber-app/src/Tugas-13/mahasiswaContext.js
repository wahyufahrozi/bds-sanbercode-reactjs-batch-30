import React, { createContext, useState, useEffect } from "react";
import axios from "axios";
export const MahasiswaContext = createContext();

export const MahasiswaProvider = (props) => {
  const [daftarMahasiswa, setDaftarMahasiswa] = useState([]);
  const [input, setInput] = useState("");
  const [currentId, setCurrentId] = useState(null);

  const fetchData = async () => {
    const result = await axios.get(
      `http://backendexample.sanbercloud.com/api/student-scores`
    );
    setDaftarMahasiswa(
      result.data.map((item) => {
        return {
          id: item.id,
          name: item.name,
          course: item.course,
          score: item.score,
        };
      })
    );
  };
  useEffect(() => {
    fetchData();
  }, []);

  return (
    <MahasiswaContext.Provider
      value={{
        daftarMahasiswa,
        setDaftarMahasiswa,
        input,
        setInput,
        currentId,
        setCurrentId,
        fetchData,
      }}
    >
      {props.children}
    </MahasiswaContext.Provider>
  );
};
