import React from "react";
import ReactDOM from "react-dom";
import "./index.css";
import App from "./App";
import "antd/dist/antd.css";

import reportWebVitals from "./reportWebVitals";
import { MahasiswaProvider } from "./Tugas-14/mahasiswaContext";
import { ThemeProvider } from "./Tugas-14/ThemeContext";
import { MahasiswaProviders } from "./Tugas-15/mahasiswaContext";

ReactDOM.render(
  <React.StrictMode>
    <MahasiswaProvider>
      <MahasiswaProviders>
        <ThemeProvider>
          <App />
        </ThemeProvider>
      </MahasiswaProviders>
    </MahasiswaProvider>
  </React.StrictMode>,
  document.getElementById("root")
);

// If you want to start measuring performance in your app, pass a function
// to log results (for example: reportWebVitals(console.log))
// or send to an analytics endpoint. Learn more: https://bit.ly/CRA-vitals
reportWebVitals();
