///SOAL 1
// buatlah variabel-variabel seperti di bawah ini

// var kataPertama = "saya";
// var kataKedua = "senang";
// var kataKetiga = "belajar";
// var kataKeempat = "javascript";
// gabungkan variabel-variabel tersebut agar menghasilkan output

// saya Senang belajaR JAVASCRIPT

var kataPertama = "saya";
var kataKedua = "senang";
var kataKetiga = "belajar";
var kataKeempat = "javascript";

console.log(
  kataPertama +
    " " +
    kataKedua.charAt(0).toUpperCase() +
    kataKedua.substr(1) +
    " " +
    kataKetiga.substr(0, 6) +
    kataKetiga.charAt(6).toUpperCase() +
    " " +
    kataKeempat.toUpperCase()
);

///Soal 2
// buatlah variabel-variabel seperti di bawah ini

// var panjangPersegiPanjang = "8";
// var lebarPersegiPanjang = "5";

// var alasSegitiga= "6";
// var tinggiSegitiga = "7";
// ubah lah variabel diatas ke dalam integer dan gunakan pada operasi perhitungan dari keliling persegi panjang dan luas segitiga dengan variabel di bawah ini:

// var kelilingPersegiPanjang;
// var luasSegitiga;

// lalu tampilkan dengan console.log

var panjangPersegiPanjang = "8";
var lebarPersegiPanjang = "5";

var alasSegitiga = "6";
var tinggiSegitiga = "7";

var kelilingPersegiPanjang =
  2 * (Number(panjangPersegiPanjang) + Number(lebarPersegiPanjang));

var luasSegitiga = 0.5 * (Number(alasSegitiga) * Number(tinggiSegitiga));

console.log(
  `keliling persegi panjang adalah : ${kelilingPersegiPanjang} Sedangkan luas segitiga adalah : ${luasSegitiga}`
);

///soal 3
// buatlah variabel-variabel seperti di bawah ini

// var sentences= 'wah javascript itu keren sekali';

// var firstWord= sentences.substring(0, 3);
// var secondWord; // do your own!
// var thirdWord; // do your own!
// var fourthWord; // do your own!
// var fifthWord; // do your own!

// console.log('Kata Pertama: ' + firstWord);
// console.log('Kata Kedua: ' + secondWord);
// console.log('Kata Ketiga: ' + thirdWord);
// console.log('Kata Keempat: ' + fourthWord);
// console.log('Kata Kelima: ' + fifthWord);
// selesaikan variabel yang belum diisi dan hasilkan output seperti berikut:

// Kata Pertama: wah
// Kata Kedua: javascript
// Kata Ketiga: itu
// Kata Keempat: keren
// Kata Kelima: sekali

var sentences = "wah javascript itu keren sekali";

var firstWord = sentences.substring(0, 3);
var secondWord = sentences.substring(4, 14);
var thirdWord = sentences.substring(15, 18);
var fourthWord = sentences.substring(19, 24);
var fifthWord = sentences.substring(25, 31);

console.log("Kata Pertama: " + firstWord);
console.log("Kata Kedua: " + secondWord);
console.log("Kata Ketiga: " + thirdWord);
console.log("Kata Keempat: " + fourthWord);
console.log("Kata Kelima: " + fifthWord);

/////soal 4

// buatlah variabel seperti di bawah ini

// var nilaiJohn = 80;
// var nilaiDoe = 50;

// tentukan indeks nilai dari nilaiJohn dan nilaiDoe (tampilkan dengan console.log) dengan kondisi :

// nilai >= 80 indeksnya A
// nilai >= 70 dan nilai < 80 indeksnya B
// nilai >= 60 dan nilai < 70 indeksnya c
// nilai >= 50 dan nilai < 60 indeksnya D
// nilai < 50 indeksnya E
// kerjakan soal ini tanpa menggunakan function(ini materi hari 5)

var nilaiJohn = 80;
var nilaiDoe = 50;
if (nilaiJohn >= 80) {
  console.log("Indeksnya A");
} else if (nilaiJohn >= 80 && nilaiJohn < 80) {
  console.log("Indeksnya B");
} else if (nilaiJohn >= 60 && nilaiJohn < 70) {
  console.log("Indeksnya C");
} else if (nilaiJohn >= 50 && nilaiJohn < 60) {
  console.log("Indeksnya D");
} else if (nilaiJohn < 50) {
  console.log("Indeksnya E");
} else {
  console.log("Tidak naik kelas");
}

if (nilaiDoe >= 80) {
  console.log("Indeksnya A");
} else if (nilaiDoe >= 80 && nilaiDoe < 80) {
  console.log("Indeksnya B");
} else if (nilaiDoe >= 60 && nilaiDoe < 70) {
  console.log("Indeksnya C");
} else if (nilaiDoe >= 50 && nilaiDoe < 60) {
  console.log("Indeksnya D");
} else if (nilaiDoe < 50) {
  console.log("Indeksnya E");
} else {
  console.log("Tidak naik kelas");
}

///Soal %

// var tanggal = 14;
// var bulan = 1;
// var tahun = 2021;
// ganti tanggal ,bulan, dan tahun sesuai dengan tanggal lahir anda dan buatlah switch case pada bulan, lalu muncul kan string nya dengan output seperti ini 22 Juli 2020 (isi di sesuaikan dengan tanggal lahir masing-masing)

var tanggal = 14;
var tahun = 1997;
var bulan = 1;
switch (bulan) {
  case 1: {
    bulan = "Januari";
    break;
  }
  case 2: {
    bulan = "Februari";
    break;
  }
  case 3: {
    bulan = "Maret";
    break;
  }
  case 4: {
    bulan = "April";
    break;
  }
  case 5: {
    bulan = "Mei";
    break;
  }
  case 6: {
    bulan = "Juni";
    break;
  }
  case 7: {
    bulan = "Juli";
    break;
  }
  case 8: {
    bulan = "Agustus";
    break;
  }
  case 9: {
    bulan = "September";
    break;
  }
  case 10: {
    bulan = "Oktober";
    break;
  }
  case 11: {
    bulan = "November";
    break;
  }
  case 12: {
    bulan = "Desember";
    break;
  }

  default:
    console.log("tidak ada bulan lagi");
    break;
}
console.log(tanggal + " " + bulan + " " + tahun);
