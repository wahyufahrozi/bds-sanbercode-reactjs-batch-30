// di callback.js
function readBooks(time, book, callback) {
  console.log("saya membaca " + book.name);
  setTimeout(function () {
    let sisaWaktu = 0;
    if (time >= book.timeSpent) {
      sisaWaktu = time - book.timeSpent;
      console.log(
        "saya sudah membaca " + book.name + ", sisa waktu saya " + sisaWaktu
      );
      callback(sisaWaktu); //menjalankan function callback
    } else {
      console.log("waktu saya habis");
      callback(time);
    }
  }, book.timeSpent);
}

module.exports = readBooks;
// Masih satu folder dengan file callback.js, buatlah sebuah file dengan nama index.js lalu tuliskan code seperti berikut.
