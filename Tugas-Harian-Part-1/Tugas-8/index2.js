// Masih di folder yang sama dengan promise.js, buatlah sebuah file dengan nama index2.js. Tuliskan code sebagai berikut

var readBooksPromise = require("./promise.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
];

const execute = (time, index) => {
  readBooksPromise(time, books[index])
    .then((sisaWaktu) => {
      if (sisaWaktu !== 1000) {
        execute(sisaWaktu, index + 1);
      }
    })
    .catch((error) => {
      console.log(error);
    });
};
execute(10000, 0);
// Lanjutkan code untuk menjalankan function readBooksPromise
// Lakukan hal yang sama dengan soal no.1, habiskan waktu selama 10000 ms (10 detik) untuk membaca semua buku dalam array books.!
