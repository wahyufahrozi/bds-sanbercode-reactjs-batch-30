var filterBooksPromise = require("./promise2.js");

filterBooksPromise(true, 40)
  .then((res) => {
    console.log(res);
  })
  .catch((err) => {
    console.log(err);
  });

async function filterBooks() {
  const data = await filterBooksPromise(false, 250);
  console.log(data);
}

filterBooks();

async function errorFilterBooks() {
  try {
    const data = await filterBooksPromise(true, 30);
    console.log(data);
  } catch (error) {
    console.log(error.message);
  }
}

errorFilterBooks();
