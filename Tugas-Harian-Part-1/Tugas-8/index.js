var readBooks = require("./callback.js");

var books = [
  { name: "LOTR", timeSpent: 3000 },
  { name: "Fidas", timeSpent: 2000 },
  { name: "Kalkulus", timeSpent: 4000 },
  { name: "komik", timeSpent: 1000 },
];

const execute = (time, index) => {
  readBooks(time, books[index], (sisaWaktu) => {
    if (sisaWaktu !== 0) {
      execute(sisaWaktu, index + 1);
    }
  });
};
execute(10000, 0);
