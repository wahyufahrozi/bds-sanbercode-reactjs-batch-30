// //Soal 1
// // Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax while. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk membuat suatu looping yang menghitung maju dan menghitung mundur. Jangan lupa tampilkan di console juga judul ‘LOOPING PERTAMA’ dan ‘LOOPING KEDUA’.”

// var i = 2;

// console.log("LOOPING PERTAMA");
// while (i <= 20) {
//   if (i % 2 === 0) {
//     console.log(`${i} - I looping Coding`);
//   }

//   i++;
// }

// console.log("LOOPING KEDUA");
// while (i >= 2) {
//   if (i % 2 === 0) {
//     console.log(`${i} - I will become a frontend developer`);
//   }

//   i--;
// }
// console.log("========================");
// //Soal 2
// // Pada tugas ini kamu diminta untuk melakukan looping dalam JavaScript dengan menggunakan syntax for. Untuk membuat tantangan ini lebih menarik, kamu juga diminta untuk memenuhi syarat tertentu yaitu:

// // SYARAT:
// // A. Jika angka ganjil maka tampilkan Santai
// // B. Jika angka genap maka tampilkan Berkualitas
// // C. Jika angka yang sedang ditampilkan adalah kelipatan 3 DAN angka ganjil maka tampilkan I Love Coding.

// for (let i = 1; i <= 20; i++) {
//   if (i % 3 === 0 && i % 2 === 1) {
//     console.log(`${i} - I Love Coding`);
//   } else if (i % 2 === 0) {
//     console.log(`${i} - Berkualitas`);
//   } else if (i % 2 === 1) {
//     console.log(`${i} - Santai`);
//   }
// }
// console.log("========================");
// //Soal 3
// // Kali ini kamu diminta untuk menampilkan sebuah segitiga dengan tanda pagar (#) dengan dimensi tinggi 7 dan alas 7. Looping boleh menggunakan syntax apa pun (while, for, do while).

// // Output:
// // #
// // ##
// // ###
// // ####
// // #####
// // ######
// // #######

// var n = 7;
// var output = "";
// for (var i = 1; i <= n; i++) {
//   output = output + "#";
//   console.log(output);
// }
// console.log("========================");
// //soal 4
// // buatlah variabel seperti di bawah ini

// // var kalimat=["aku", "saya", "sangat", "sangat", "senang", "belajar", "javascript"]
// // ubah kalimat diatas menjadi seperti di bawah ini:

// // ["saya", "sangat", "senang", "belajar", "javascript"]

// // lalu tampilkan dengan output seperti di bawah ini:

// // "saya sangat senang belajar javascript"

// var kalimat = [
//   "aku",
//   "saya",
//   "sangat",
//   "sangat",
//   "senang",
//   "belajar",
//   "javascript",
// ];
// kalimat.shift();
// kalimat.splice(1, 1);
// var text = kalimat.join(" ");
// console.log(text);
// console.log("========================");
// //Soal 5
// // var sayuran=[]
// // perlu di ingat bahwa deklarasi variabel sayuran diatas di mulai dengan array kosong dulu

// // tambahkanlah data di bawah ini ke variabel sayuran:

// // Kangkung
// // Bayam
// // Buncis
// // Kubis
// // Timun
// // Seledri
// // Tauge
// // lalu urutkan berdasarkan alfabet dan lalu tampilkan dengan loop dan beri angka di depannya sehingga menghasilkan output seperti ini:

// // 1. Bayam
// // 2. Buncis
// // 3. Kangkung
// // 4. Kubis
// // 5. Seledri
// // 6. Tauge
// // 7. Timun
// var sayuran = [];

// sayuran.push(
//   "Kangkung",
//   "Bayam",
//   "Buncis",
//   "Kubis",
//   "Timun",
//   "Seledri",
//   "Tauge"
// );

// sayuran.sort();
// for (let i = 0; i < sayuran.length; i++) {
//   console.log(i + 1, sayuran[i]);
// }
// console.log("========================");

let arr = [5, 1, 2, 31];
let tmp;

function arrAscending(params) {
  return new Promise((resolve, reject) => {
    for (let i = 0; i < arr.length; i++) {
      for (let j = i + 1; j < arr.length; j++) {
        if (arr[i] > arr[j]) {
          tmp = arr[i];
          arr[i] = arr[j];
          arr[j] = tmp;
        }
      }
    }
    resolve(arr);
  });
}

arrAscending(arr).then((e) => {
  console.log(e);
});
