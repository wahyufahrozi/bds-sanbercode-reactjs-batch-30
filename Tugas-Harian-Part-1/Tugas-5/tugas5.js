// //soal 1
// Tulislah 3 function dengan nama luas persegi panjang, keliling persegi panjang dan volume balok

// /*
//     Tulis code function di sini
// */

// var panjang= 12
// var lebar= 4
// var tinggi = 8

// var luasPersegiPanjang = luasPersegiPanjang(panjang, lebar)
// var kelilingPersegiPanjang = kelilingPersegiPanjang(panjang, lebar)
// var volumeBalok = volumeBalok(panjang, lebar, tinggi)

// console.log(luasPersegiPanjang)
// console.log(kelilingPersegiPanjang)
// console.log(volumeBalok)

var panjang = 12;
var lebar = 4;
var tinggi = 8;
var luaspersegipanjang = function (panjang, lebar) {
  return panjang * lebar;
};

var kelilingPersegiPanjang = function (panjang, lebar) {
  return 2 * panjang + 2 * lebar;
};

var volumeBalok = function (panjang, lebar, tinggi) {
  return panjang * lebar * tinggi;
};
console.log(volumeBalok(panjang, lebar, tinggi));
console.log(luaspersegipanjang(panjang, lebar));
console.log(kelilingPersegiPanjang(panjang, lebar));
console.log("=====================");
// soal 2
// Tulislah sebuah function dengan nama introduce() yang memproses paramater yang dikirim menjadi sebuah kalimat perkenalan seperti berikut: “Nama saya [nama], umur saya [umur] tahun, alamat saya di [alamat], dan saya punya hobby yaitu [hobi]!”

var introduce = function (name, age, address, hobby) {
  return (
    "Nama saya" +
    " " +
    name +
    " umur saya" +
    " " +
    age +
    " " +
    "tahun, alamat saya di" +
    " " +
    address +
    ", dan saya punya hobby yaitu" +
    " " +
    hobby
  );
};

var name = "John";
var age = 30;
var address = "Jalan belum jadi";
var hobby = "Gaming";

var perkenalan = introduce(name, age, address, hobby);
console.log(perkenalan);
console.log("=====================");
// Menampilkan "Nama saya John, umur saya 30 tahun, alamat saya di Jalan belum jadi, dan saya punya hobby yaitu Gaming!"

// soal 3
// ubahlah array di bawah ini menjadi object dengan property nama, jenis kelamin, hobi dan tahun lahir (var arrayDaftarPeserta harus di olah menjadi object)

var arrayDaftarPeserta = ["John Doe", "laki-laki", "baca buku", 1992];

var objDaftarPeserta = {
  nama: arrayDaftarPeserta[0],
  "jenis kelamin": arrayDaftarPeserta[1],
  hobi: arrayDaftarPeserta[2],
  "tahun lahir": arrayDaftarPeserta[3],
};
console.log(objDaftarPeserta);
console.log("=====================");

// soal 4
// anda diberikan data-data buah seperti di bawah ini

// 1.nama: Nanas
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 9000
// 2.nama: Jeruk
//   warna: Oranye
//   ada bijinya: ada
//   harga: 8000
// 3.nama: Semangka
//   warna: Hijau & Merah
//   ada bijinya: ada
//   harga: 10000
// 4.nama: Pisang
//   warna: Kuning
//   ada bijinya: tidak
//   harga: 5000
// uraikan data tersebut menjadi array of object dan munculkan data yang tidak memiliki biji
var fruits = [
  {
    nama: "Nanas",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 9000,
  },
  { nama: "Jeruk", warna: "Oranye", "ada bijinya": "ada", harga: 8000 },
  {
    nama: "Semangka",
    warna: "Hijau & Merah",
    "ada bijinya": "ada",
    harga: 10000,
  },
  {
    nama: "Pisang",
    warna: "Kuning",
    "ada bijinya": "tidak",
    harga: 5000,
  },
];

var fruitsFilter = fruits.filter(function (item) {
  return item["ada bijinya"] === "tidak";
});

console.log(fruitsFilter);
console.log("=====================");

// soal 5
// buatlah function tambahDataFilm yang menambahkan object ke array

var tambahDataFilm = function (a, b, c, d) {
  var obj = {
    judul: a,
    durasi: b,
    genre: c,
    tahun: d,
  };
  dataFilm.push(obj);
};
var dataFilm = [];
tambahDataFilm("LOTR", "2 jam", "action", "1999");
tambahDataFilm("avenger", "2 jam", "action", "2019");
tambahDataFilm("spiderman", "2 jam", "action", "2004");
tambahDataFilm("juon", "2 jam", "horror", "2004");
console.log(dataFilm);
console.log("=====================");
