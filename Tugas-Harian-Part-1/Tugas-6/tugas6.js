// Soal 1
// buatlah dua fungsi yaitu fungsi luas lingkaran dan keliling lingkaran dengan arrow function (wajib ada parameternya)  lalu gunakan let atau const di dalam soal ini
const luasLingkaran = (jariJari, pi = 3.14) => {
  return Math.ceil(pi * (jariJari * jariJari));
};
console.log(luasLingkaran(8));

const kelilingLingkaran = (diameter, pi = 3.14) => {
  return Math.ceil(pi * diameter);
};

console.log(kelilingLingkaran(28));

// Soal 2
// Tulislah sebuah arrow function dengan nama introduce yang parameternya menggunakan rest parameter dan menghasilkan kalimat "Pak John adalah seorang penulis yang berusia 30 tahun" menggunakan template literal.

// pastikan semua parameter pada function introduce di gunakan semuanya

const introduce = (...params) => {
  let [nama, age, genre, job] = params;

  let tmp = "undefined";

  if (genre === "Laki-Laki") {
    tmp = "Pak";
  } else {
    tmp = "Buk";
  }
  return `${tmp} ${nama} adalah seorang ${job} yang berusia ${age} tahun`;
};

//kode di bawah ini jangan dirubah atau dihapus
const perkenalan = introduce("John", "30", "Laki-Laki", "penulis");
console.log(perkenalan); // Menampilkan "Pak John adalah seorang penulis yang berusia 30 tahun"
// NOTE : Semua parameter harus digunakan didalam function

// Soal 3
// return dalam fungsi di bawah ini masih menggunakan object literal dalam ES5, ubahlah menjadi bentuk yang lebih sederhana di ES6.

const newFunction = (firstName, lastName) => {
  return {
    firstName,
    lastName,
    fullName: () => {
      console.log(firstName, lastName);
    },
  };
};
// kode di bawah ini jangan diubah atau dihapus sama sekali
console.log(newFunction("John", "Doe").firstName);
console.log(newFunction("Richard", "Roe").lastName);
newFunction("William", "Imoh").fullName();

// Soal 4
// Diberikan sebuah objek sebagai berikut:

let phone = {
  name: "Galaxy Note 20",
  brand: "Samsung",
  year: 2020,
  colors: ["Mystic Bronze", "Mystic White", "Mystic Black"],
};
// kode diatas ini jangan di rubah atau di hapus sama sekali

// const phoneBrand = phone.brand;
// const phoneName = phone.name;
const { name, brand, year } = phone;
const [colorBronze, colorWhite, colorBlack] = phone.colors;
// const colorBlack = phone.colors[2];
// const colorBronze = phone.colors[0];
// kode di bawah ini jangan dirubah atau dihapus
// console.log(Phonename, Phonebrand, year, colorBlack, colorBronze);
// tuliskan kode jawaban yang berisi hasil destructuring yang nantinya akan di gunakan dalam console.log
console.log(name, brand, year, colorBlack, colorBronze);

// soal 5
// buatlah variabel-variabel seperti di bawah ini:

let warna = ["biru", "merah", "kuning", "hijau"];

let dataBukuTambahan = {
  penulis: "john doe",
  tahunTerbit: 2020,
};

let buku = {
  nama: "pemograman dasar",
  jumlahHalaman: 172,
  warnaSampul: ["hitam"],
};
// kode diatas ini jangan di rubah atau di hapus sama sekali

/* Tulis kode jawabannya di sini */

warnaSampul = [...buku.warnaSampul, ...warna];

let newArray = { ...buku, warnaSampul, ...dataBukuTambahan };
console.log(newArray);
// gabungkanlah variabel warna (gabungkan dengan atribut warnaSampul) dan dataBukuTambahan ke variabel buku dengan menggunakan spread operator
